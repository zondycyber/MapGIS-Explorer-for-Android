package com.zondy.treebean;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.core.map.GroupLayer;
import com.zondy.mapgis.core.map.ServerLayer;
import com.zondy.mapgis.explorer.R;

public class SimpleTreeAdapter<T> extends TreeListViewAdapter<T>
{

	public SimpleTreeAdapter(ListView mTree, Context context, List<T> datas, int defaultExpandLevel,MapView mapview) throws IllegalArgumentException, IllegalAccessException
	{
		super(mTree, context, datas, defaultExpandLevel, mapview);
	}

	@Override
	public View getConvertView(final Node node, int position, View convertView, ViewGroup parent)
	{

		ViewHolder viewHolder = null;
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.mapmanager_list_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.id_treenode_icon);
			viewHolder.imgView = (ImageView) convertView.findViewById(R.id.common_img);
			viewHolder.nameText = (TextView) convertView.findViewById(R.id.common_fieldname_s);
			viewHolder.valueText = (TextView) convertView.findViewById(R.id.common_fieldvalue);
			viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.common_select_check);

			// here

			convertView.setTag(viewHolder);

		}
		else
		{
			viewHolder = (ViewHolder) convertView.getTag();
		}

		//ico
		if (node.getIcon() == -1)
		{
			viewHolder.icon.setVisibility(View.INVISIBLE);
		} else
		{
			viewHolder.icon.setVisibility(View.VISIBLE);
			viewHolder.icon.setImageResource(node.getIcon());
		}
		
		// checkbox
		viewHolder.checkBox.setClickable(false);
		if (node.getMaplayer().getIsVisible())
		{
			viewHolder.checkBox.setChecked(true);
		}
		else
		{
			viewHolder.checkBox.setChecked(false);
		}
		// img
		if (!(node.getMaplayer() instanceof ServerLayer) && node.getMaplayer() instanceof GroupLayer)
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.folder);
		}
		else
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.file);
		}

		// name
		viewHolder.nameText.setText(node.getName());

		return convertView;
	}

	private final class ViewHolder
	{
		public ImageView icon;
		public ImageView imgView;
		public TextView nameText;
		public TextView valueText;
		public CheckBox checkBox;
	}

}
