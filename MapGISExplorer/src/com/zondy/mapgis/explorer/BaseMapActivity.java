package com.zondy.mapgis.explorer;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;

import com.zondy.mapgis.android.base.BaseActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.map.dao.IActivityLife;
import com.zondy.mapgis.android.map.dao.ICallBack;
import com.zondy.mapgis.android.model.EnumViewPos;
import com.zondy.mapgis.map.model.DRect;

/**
 * 基础地图{@link Activity}。继承自{@link BaseActivity}<br>
 * 定义了一组接口，和部分接口的默认实现，不包含任何界面相关操作。 其子类应当实现所有接口。 功能与界面关联起。
 * 
 * @author fjl
 */
public abstract class BaseMapActivity extends BaseActivity
{

	protected static BaseMapActivity activity;
	protected static DRect rect;
	protected boolean isFirstLoad;

	protected List<IActivityLife> lstLife = new ArrayList<IActivityLife>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		activity = this;

		isFirstLoad = true;
		for (IActivityLife life : lstLife)
		{
			life.onCreate();
		}
	}

	/**
	 * 初始化窗体
	 */
	public abstract void initView();

	/**
	 * 初始化地图
	 */
	public abstract void initMap();


	/**
	 * 添加Fragment
	 * 
	 * @param enumPos Fragment的位置
	 * @param frag Fragment
	 * @param strTag Fragment的名称
	 * @param callback 可自定义的接口
	 * @return
	 */
	public abstract void addFragment(EnumViewPos enumPos, BaseFragment frag, String strTag, ICallBack callback);


	/**
	 * 按返回键时调用该方法。
	 * 
	 * @return
	 */
	public abstract boolean goback();

	/**
	 * 取地图Activity实例
	 * 
	 * @return
	 */
	public static BaseMapActivity getInstance()
	{
		return activity;
	}

	/**
	 * 注册生命周期监听事件
	 * 
	 * @param life
	 */
	public void registerLife(IActivityLife life)
	{
		this.lstLife.add(life);
	}

	/**
	 * 取消注册生命周期监听事件
	 * 
	 * @param life
	 */
	public void unRegisterLife(IActivityLife life)
	{
		this.lstLife.remove(life);
	}

	@Override
	protected void onRestart()
	{
		super.onRestart();
		isFirstLoad = false;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		for (IActivityLife life : lstLife)
		{
			life.onResume();
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		for (IActivityLife life : lstLife)
		{
			life.onDestroy();
		}
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		for (IActivityLife life : lstLife)
		{
			life.onStart();
		}
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		for (IActivityLife life : lstLife)
		{
			life.onStop();
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		for (IActivityLife life : lstLife)
		{
			life.onPause();
		}
	}

	/**
	 * 刷新一下各种绘图图元的可见性<br>
	 * 如坐标等
	 */

	/**
	 * 刷新一下在指定放大级数上的各种绘图图元的可见性
	 * 
	 * @param zoom
	 */

	/**
	 * 首次加载地图后地图放大到 rect 范围
	 * 
	 * @param rect 空间范围
	 */

	/**
	 * 取首次加载地图后放大的范围
	 */

	/**
	 * 是否第一次创建
	 * 
	 * @return
	 */

	/**
	 * 获取地图视图
	 * 
	 * @return
	 */

	/**
	 * 取MapGIS版本的地图视图。
	 * 
	 * @return
	 */

	/**
	 * 清除地图视图上所有交互事件
	 */

	/**
	 * 清除地图上的点击事件
	 */

	/**
	 * 清除地图上的长按事件
	 */

	/**
	 * 清除地图上的触摸事件
	 */

	/**
	 * 设置地图的点击事件
	 * 
	 * @param oncliCk
	 */

}
