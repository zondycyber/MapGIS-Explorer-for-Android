package com.zondy.mapgis.map.model;


import com.zondy.mapgis.core.attr.FieldType;

/**
 * 字段类型
 */
public enum LandFieldType {

	/**
	 * 字符串型
	 */
	fldString("字符串型"),
	/**
	 * 整型
	 */
	fldInt("整型"),
	/**
	 * 浮点型
	 */
	fldFloat("浮点型"),
	/**
	 * 日期型
	 */
	fldDate("日期型");

	private String cn;

	private LandFieldType(String StrCn) {
		this.cn = StrCn;
	}
	public static LandFieldType fromStringEn(String str) {

		LandFieldType fldType;
		if (str.equalsIgnoreCase("fldFloat")) {
			fldType = LandFieldType.fldFloat;
		} else if (str.equalsIgnoreCase("fldInt")) {
			fldType = LandFieldType.fldInt;
		} else if(str.equalsIgnoreCase("fldDate")){
			fldType = LandFieldType.fldDate;
		} else {
			fldType = LandFieldType.fldString;
		}
		return fldType;
	}
	
	@Override
	public String toString() {
		switch (this) {
		case fldString:
			return "fldString";
		case fldInt:
			return "fldInt";
		case fldFloat:
			return "fldFloat";
		case fldDate:
			return "fldDate";
		default:
			return "";
		}
	}
//	public static LandFieldType fromStringCn(String strCn)
//	{
//		return new  LandFieldType(strCn);
//	}
	public String toStringCn()
	{
		return cn;
	}

	/**
	 * 将平台的FieldType转换为国土的LandFieldType，只支持部分字段类型的转换
	 * @param fldType
	 * @return 日期、Blob等复杂类型的字段暂不支持，一律使用fldString
	 */
	public static LandFieldType fromFieldType(FieldType fldType) {

		LandFieldType landFldType = fldString;
		switch (fldType.value()) {

		case 6:
		case 7:
		case 8:
			landFldType = LandFieldType.fldFloat;
			break;
		case 1:
		case 3:
		case 4:
		case 5:
			landFldType = LandFieldType.fldInt;
			break;
		case 0:
			landFldType = LandFieldType.fldString;
			break;
		case 9:
			landFldType = LandFieldType.fldDate;
			break;
		default:
			break;
		}
		return landFldType;
	}

	/*
	 * int
	 * esriFieldTypeBlob
	 * define an esri Blob field.
	 * 
	 * int
	 * esriFieldTypeDate
	 * define an esri Date field.
	 * 
	 * int
	 * esriFieldTypeDouble
	 * define an esri double field.
	 * 
	 * int
	 * esriFieldTypeGUID
	 * define an esri GUID field.
	 * 
	 * int
	 * esriFieldTypeGeometry
	 * define an esri geometry field.
	 * 
	 * int
	 * esriFieldTypeGlobalID
	 * define an esri Gloval ID field.
	 * 
	 * int
	 * esriFieldTypeInteger
	 * define an esri integer field.
	 * 
	 * int
	 * esriFieldTypeOID
	 * define an esri Object ID field.
	 * 
	 * int
	 * esriFieldTypeRaster
	 * define an esri Raster field.
	 * 
	 * int
	 * esriFieldTypeSingle
	 * define an esri single field.
	 * 
	 * int
	 * esriFieldTypeSmallInteger
	 * define an esri small integer field.
	 * 
	 * int
	 * esriFieldTypeString
	 * define an esri String field.
	 * 
	 * int
	 * esriFieldTypeXML
	 * define an esri Raster field.
	 */

	/**
	 * 从ArcGIS的字段类型转为LandFieldType，只支持部分字段类型的转换
	 * @param fldType
	 * @return  日期、Blob等复杂类型的字段暂不支持，一律使用fldString
	 */
//	public static LandFieldType fromArcFieldType(int fldType) {
//
//		LandFieldType landFldType = fldString;
//		switch (fldType) {
//		case Field.esriFieldTypeDouble:
//			landFldType = fldFloat;
//			break;
//
//		case Field.esriFieldTypeSmallInteger:
//		case Field.esriFieldTypeInteger:
//			landFldType = fldInt;
//			break;
//
//		case Field.esriFieldTypeString:
//			landFldType = fldString;
//			break;
//		default:
//			break;
//		}
//		return landFldType;
//	}

}
