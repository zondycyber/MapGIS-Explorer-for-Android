package com.zondy.mapgis.map.model;

import com.zondy.mapgis.core.geometry.Rect;

/**
 * @content 自定义的矩形
 * @author admin 2016-6-2 下午5:22:12
 */
public class DRect{
	
	public double xmin;
	public double ymin;
	public double xmax;
	public double ymax;
	
	public DRect(){
		xmin = 0;
		xmax = 0;
		ymin = 0;
		ymax = 0;
	}
	
	public DRect(Rect rect){
		
		this.xmin = rect.xMin;
		this.xmax = rect.xMax;
		this.ymin = rect.yMin;
		this.ymax = rect.yMax;
	}
	
	public DRect(double xmin,double xmax,double ymin,double ymax){
		
		this.xmin = xmin;
		this.ymin = ymin;
		this.xmax = xmax;
		this.ymax = ymax;
	}
	
	/**
	 * 根据标准格式的字符串创建一个实例
	 * @param strRect  格式为 xmin,xmax,ymin,ymax
	 */
	public DRect(String strRect){
		
		String[] strCoorArray = strRect.split(",");
		this.xmin = Double.valueOf(strCoorArray[0]);
		this.xmax = Double.valueOf(strCoorArray[1]);
		this.ymin = Double.valueOf(strCoorArray[2]);
		this.ymax = Double.valueOf(strCoorArray[3]);
	}
	

	public double getXmin() {
		return xmin;
	}
	public void setXmin(double xmin) {
		this.xmin = xmin;
	}
	public double getYmin() {
		return ymin;
	}
	public void setYmin(double ymin) {
		this.ymin = ymin;
	}
	public double getXmax() {
		return xmax;
	}
	public void setXmax(double xmax) {
		this.xmax = xmax;
	}
	public double getYmax() {
		return ymax;
	}
	public void setYmax(double ymax) {
		this.ymax = ymax;
	}
	
	/**
	 * 获取MapGIS版本的矩形
	 * @return
	 */
	public Rect toRect(){
		
		Rect rect = new Rect();
		rect.setXMax(xmax);
		rect.setXMin(xmin);
		rect.setYMax(ymax);
		rect.setYMin(ymin);
		return rect;
	}
	
	
	/**
	 * 按比例放大矩形
	 * @param zoom	放大比例
	 * @return	放大后的矩形，如果当前矩形或放大倍数zoom不合法，则返回 null
	 */
	public DRect zoom(double zoom){
		
		if(null == this || zoom <= 0){
			return null;
		}
		if(this.getXmax() < this.getXmin() || this.getYmax() < this.getYmin()){
			return null;
		}
		
		DRect rectOut = new DRect();
		double rectInXmax = this.getXmax();
		double rectInXmin = this.getXmin();
		double rectInYMax = this.getYmax();
		double rectInYMin = this.getYmin();
		
		double widthIn = rectInXmax - rectInXmin;
		double heightIn = rectInYMax - rectInYMin;
		
		double widthOut = widthIn * zoom;
		double heightOut = heightIn * zoom;
		
		double rectOutXmax = rectInXmax + (widthOut - widthIn) / 2;
		double rectOutXmin = rectInXmin - (widthOut - widthIn) / 2;
		double rectOutYmax = rectInYMax + (heightOut - heightIn) / 2;
		double rectOutYmin = rectInYMin - (heightOut - heightIn) / 2;
		
		rectOut.setXmax(rectOutXmax);
		rectOut.setXmin(rectOutXmin);
		rectOut.setYmax(rectOutYmax);
		rectOut.setYmin(rectOutYmin);
		
		return rectOut;
	
		
	}

	/**
	 * 是否为默认矩形
	 * @return
	 */
	public boolean isOriginal(){
		if(Math.abs(xmin) < 0.00000001 && Math.abs(xmin) < 0.00000001
					&& Math.abs(xmin) < 0.00000001 && Math.abs(xmin) < 0.00000001){
			return true;
		}
		return false;
	}
	
	/**
	 * 返回字符串格式   xmin,xmax,ymin,ymax
	 */
	@Override
	public String toString() {
		
		String strRect = xmin + "," + xmax + "," + ymin + "," + ymax;
		return strRect;
	}
	
	/**
	 * 矩形是否包含某一个点，带容差 
	 * @param x
	 * @param y
	 * @param tolerant 容差
	 * @return
	 */
	public boolean contain(double x, double y, double tolerant){
		
		DRect rect = new DRect(xmin - tolerant, xmax + tolerant, ymin - tolerant, ymax + tolerant);
		return rect.contain(x, y);
	}
	
	/**
	 * 矩形是否包含某一个点，精确判断
	 * @param x
	 * @param y
	 * @return 点在矩形内或边界上，返回true，否则返回false
	 */
	public boolean contain(double x, double y){
		
		return x >= xmin && x <= xmax && y >= ymin && y <= ymax;
	}
	
	/**
	 * 判断当前矩形是否完全包含drect
	 * @param drect
	 * @return
	 */
	public boolean contain(DRect drect){
		
		return this.xmax >= drect.xmax && this.xmin <= drect.xmin
					&& this.ymin <= drect.ymin && this.ymax >= drect.ymax;
	}

}
