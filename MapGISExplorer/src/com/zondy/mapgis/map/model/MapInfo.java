package com.zondy.mapgis.map.model;

import java.io.Serializable;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.core.map.MapServerType;

/**
 * @content 加载地图对象
 * @author fjl 2016-7-25 下午5:54:27
 */
public class MapInfo implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	private int id = 0;
	private int mapType = MapActivity.ONLINEMAP;//加载地图类型
	private String mapxPath = null;//地图文档（max路径）
	private String igserverAddress = null;//IGServer服务地址
	private String mapServerType = MapServerType.OpenStreetStandard.name();//在线地图服务类型
	private String serverUrl = "";
	private String serverName = "";

	public MapInfo()
	{
		
	}
	
	public MapInfo(int id ,int mapType ,String mapxPath,String igserverAddress,String mapServerType,String serverUrl,String serverName)
	{
		super();
		this.id = id;
		this.mapType = mapType;
		this.mapxPath = mapxPath;
		this.igserverAddress = igserverAddress;
		this.mapServerType = mapServerType;
		this.serverUrl = serverUrl;
		this.serverName = serverName;
	}
	
	public int getMapType()
	{
		return mapType;
	}
	public void setMapType(int mapType)
	{
		this.mapType = mapType;
	}
	public String getMapxPath()
	{
		return mapxPath;
	}
	public void setMapxPath(String mapxPath)
	{
		this.mapxPath = mapxPath;
	}
	public String getIgserverAddress()
	{
		return igserverAddress;
	}
	public void setIgserverAddress(String igserverAddress)
	{
		this.igserverAddress = igserverAddress;
	}
	public String getMapServerType()
	{
		return mapServerType;
	}
	public void setMapServerType(String mapServerType)
	{
		this.mapServerType = mapServerType;
	}
	public String getServerUrl()
	{
		return serverUrl;
	}

	public void setServerUrl(String serverUrl)
	{
		this.serverUrl = serverUrl;
	}
	
	public String getServerName()
	{
		return serverName;
	}
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
}
