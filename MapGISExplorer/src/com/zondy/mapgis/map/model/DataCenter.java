package com.zondy.mapgis.map.model;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.util.Log;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.map.dao.IListener;
import com.zondy.mapgis.core.featureservice.Feature;
import com.zondy.mapgis.core.featureservice.FeaturePagedResult;
import com.zondy.mapgis.core.featureservice.FeatureQuery;
import com.zondy.mapgis.core.featureservice.FeatureQuery.QueryBound;
import com.zondy.mapgis.core.geometry.Dot;
import com.zondy.mapgis.core.geometry.GeoPolygon;
import com.zondy.mapgis.core.geometry.Geometry;
import com.zondy.mapgis.core.map.VectorLayer;

public class DataCenter
{
	private static volatile DataCenter dataCenter = null;
	private ExecutorService threadPool;
	private Lock lock = new ReentrantLock();

	/**
	 * 获取DataCenter实例
	 * 
	 * @return
	 */
	public static DataCenter getInstance()
	{
		if (dataCenter == null)
		{
			synchronized (DataCenter.class)
			{
				if (dataCenter == null)
				{
					dataCenter = new DataCenter();
				}
			}
		}
		return dataCenter;
	}

	/**
	 * 初始化
	 * 
	 * @return
	 */
	public DataCenter init()
	{
		this.threadPool = Executors.newCachedThreadPool();
		return dataCenter;

	}
	
	/**
	 * 空间属性混合查询（回调方式返回查询结果）
	 * 
	 * @param mapActivity
	 * @param queryCondiotion
	 * @param resultListen
	 */
	public void mapLayerAttMultiSearch(MapActivity mapActivity, final QueryCondition queryCondiotion, final IListener<FeaturePagedResult> resultListen)
	{

		threadPool.execute(new Runnable()
		{
			@Override
			public void run()
			{
				lock.lock();
//				FeatureQuery featureQuery = new FeatureQuery();
				// 设置查询条件
				VectorLayer vctLayer = (VectorLayer) queryCondiotion.getmMapLayer();
				Log.d("DataCenter", vctLayer.getName());
				Log.d("DataCenter", "queryCondiotion:"+queryCondiotion.getmMapLayer());
				Dot[] dots = queryCondiotion.getDots();
				String strwhere = queryCondiotion.getStrWhere();

//				featureQuery.setVectorLayer(vctLayer);
				FeatureQuery featureQuery = new FeatureQuery(vctLayer);
				featureQuery.setWhereClause(strwhere);
				featureQuery.setQueryBound(new QueryBound(dots));
				featureQuery.setReturnAttribute(true);
				featureQuery.setReturnGeometry(true);
				FeaturePagedResult result = featureQuery.query();
				
				//test
//				int count = result.getPageCount();
//				List<Feature> fealst = result.getPage(1);
				resultListen.onCall(result);
				
				lock.unlock();
			}
		});

	}


	/**
	 * 空间属性混合查询（回调方式返回查询结果）
	 * 
	 * @param mapActivity
	 * @param queryCondiotion
	 * @param resultListen
	 */
	public void showAttMultiSearch(MapActivity mapActivity, final QueryCondition queryCondiotion, final IListener<FeaturePagedResult> resultListen)
	{

		threadPool.execute(new Runnable()
		{
			@Override
			public void run()
			{
				lock.lock();
				// 设置查询条件
				Layer baseLayer = (Layer) queryCondiotion.getLayer();
				Log.e("Layer","baseLayer:"+ baseLayer);
				VectorLayer vctLayer = (VectorLayer) baseLayer.getMapLayer();
				Dot[] dots = queryCondiotion.getDots();
				String strwhere = queryCondiotion.getStrWhere();

				FeatureQuery featureQuery = new FeatureQuery(vctLayer);
				featureQuery.setWhereClause(strwhere);
				featureQuery.setQueryBound(new QueryBound(dots));
				featureQuery.setReturnAttribute(true);
				featureQuery.setReturnGeometry(true);
				FeaturePagedResult result = featureQuery.query();
				resultListen.onCall(result);

				lock.unlock();
			}
		});

	}
	
	
	public static void featureToGeometory(List<Feature> featureLst)
	{
		for(int i = 0;i < featureLst.size();i++)
		{
			Feature feature = featureLst.get(i);
			Geometry geometry = feature.getGeometry();
			if(geometry instanceof GeoPolygon)
			{
				GeoPolygon geopolygon = (GeoPolygon) geometry;
				
				
			}
		
			
		}
		
		
	}
	
	public static void drawOnMap()
	{
		
	}

}
