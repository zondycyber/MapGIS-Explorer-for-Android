package com.zondy.mapgis.android.utils;

import com.zondy.mapgis.core.geometry.Dot;
import com.zondy.mapgis.core.geometry.Rect;
import com.zondy.mapgis.core.spatial.SpaProjection;


/**
 * tw 2012-11-16
 * 
 * @author thinkpad
 * 
 */
public class PointTransferUtil {

	/*
	 * 
	 * @params 经纬度
	 * 
	 * @return google坐标 经纬度转为google坐标
	 */
	public static Dot Lonlat2Mercator(double lon, double lat) {
		Dot dot = new Dot();
		double x = lon * 20037508.34 / 180;
		double yy = Math.tan((90 + lat) * Math.PI / 360);
		double y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
		y = y * 20037508.34 / 180;
		dot.setX(x);
		dot.setY(y);
		return dot;
	}

	/*
	 * 
	 * @params 经纬度
	 * 
	 * @return google坐标 经纬度转为google坐标
	 */
	public static Dot Lonlat2Mercator(Dot lonlat) {
		if(lonlat.x > 180 || lonlat.y > 90)
		{
			return lonlat;
		}
		Dot dot = new Dot();
		double x = lonlat.getX() * 20037508.34 / 180;
		double y = Math.log(Math.tan((90 + lonlat.getY()) * Math.PI / 360))
				/ (Math.PI / 180);
		y = y * 20037508.34 / 180;
		dot.setX(x);
		dot.setY(y);
		return dot;
	}

	/*
	 * mercator投影转为经纬度
	 */
	public static Dot Mercator2Lonlat(Dot dot) {
		Dot latlon = new Dot();
		latlon.setX(dot.getX());
		latlon.setY(dot.getY());
		SpaProjection sp = new SpaProjection();
//		sp.setSourcePara(SpaProjection.getWebMercatorSrs());
//		sp.setDestinationPara(SpaProjection.getWGS84Srs());
//		sp.projTrans(latlon);
		sp.mercator2LonLat(latlon);
		return latlon;
	}

	/*
	 * Wgs84坐标转为mercator投影
	 */
//	public static Dot Wgs84ToMercator(double gpsx, double gpsy) {
//		Dot gpsdot = new Dot();
//		gpsdot.setX(gpsx);
//		gpsdot.setY(gpsy);
//		SpaProjection sp = new SpaProjection();
//		sp.setSourcePara(SpaProjection.getWGS84Srs());
//		sp.setDestinationPara(SpaProjection.getWebMercatorSrs());
//		sp.projTrans(gpsdot);
//		return gpsdot;
//	}
	
	public static Rect Lonlat2MercatorRect(Rect rect) {
		Dot[] dots = new Dot[4];
		dots[0] = Lonlat2Mercator(new Dot(rect.getXMin(), rect.getYMax()));
		dots[1] = Lonlat2Mercator(new Dot(rect.getXMin(), rect.getYMin()));
		dots[2] = Lonlat2Mercator(new Dot(rect.getXMax(), rect.getYMax()));
		dots[3] = Lonlat2Mercator(new Dot(rect.getXMax(), rect.getYMin()));
		double xmin = dots[0].getX();
		double xmax = dots[0].getX();
		double ymin = dots[0].getY();
		double ymax = dots[0].getY();
		for (int i = 1; i < 4; ++i){
			if (xmin > dots[i].getX()){
				xmin = dots[i].getX();
			}
			if (xmax < dots[i].getX()) {
				xmax = dots[i].getX();
			}
			if (ymin > dots[i].getY()){
				ymin = dots[i].getY();
			}
			
			if (ymax < dots[i].getY()){
				ymax = dots[i].getY();
			}
		}
		
		return zoomRect(new Rect(xmin,ymin,xmax,ymax), 1.5);
	}
	
	public static Rect zoomRect(Rect rect, double rate){
		Dot centerDot = getCenterDot(rect);
		double x1 = rect.xMax - rect.xMin;
		double y1 = rect.yMax - rect.yMin;
		
		double x2 = rate * x1;
		double y2 = rate * y1;
		
		Rect desRect = new Rect();
		desRect.setXMax(centerDot.x + x2/2);
		desRect.setXMin(centerDot.x - x2/2);
		desRect.setYMax(centerDot.y + y2/2);
		desRect.setYMin(centerDot.y - y2/2);
		return desRect;
	}
	
	private static Dot getCenterDot(Rect rect) {
		if (rect == null)
			return null;
		Dot dot = new Dot();
		double x = rect.xMin / 2 + rect.xMax / 2;
		double y = rect.yMax / 2 + rect.yMin / 2;
		dot.setX(x);
		dot.setY(y);
		return dot;
	}

	public static double DegreeToMeter(double degree) {
		return degree * (2 * Math.PI * 6378137.0) / 360; // 度转成米
	}

	public static double MeterToDegree(double meter) {
		return meter * 360 / (2 * Math.PI * 6378137.0); // 米转成度
	}

//	public static double distance(Dot dotA, Dot dotB) {
//		return SpaCalculator.distance(dotA, dotB);
//	}
}
