package com.zondy.mapgis.android.utils;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.zondy.mapgis.android.activity.GPSCenterActivity;
import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.frag.MeasureAreaFrag;
import com.zondy.mapgis.android.frag.MeasureLengthFrag;
import com.zondy.mapgis.android.frag.RecordTracksFrag;
import com.zondy.mapgis.android.model.EnumViewPos;

public class AnimationsToolsUtils{
	
	private static int	xOffset		= 0;
	private static int	yOffset		= 0;

	public static void initOffset(Context context){//由布局文件
		xOffset		= (int) (10.667 *context.getResources().getDisplayMetrics().density);
		yOffset		= -(int) (10.667 *context.getResources().getDisplayMetrics().density);
	}
	
	public static Animation getRotateAnimation(float fromDegrees ,float toDegrees,int durationMillis){
		RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(durationMillis);
		rotate.setFillAfter(true);
		return rotate;
	}

	public static void startAnimationsIn(ViewGroup viewgroup,int durationMillis) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
				ImageButton inoutimagebutton = (ImageButton) viewgroup
						.getChildAt(i);
				inoutimagebutton.setVisibility(0);
				MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton.getLayoutParams();
				Animation animation = new TranslateAnimation(mlp.rightMargin-xOffset,0F,yOffset + mlp.bottomMargin, 0F);
				
				animation.setFillAfter(true);animation.setDuration(durationMillis);
				animation.setStartOffset((i * 100)
						/ (-1 + viewgroup.getChildCount()));
				animation.setInterpolator(new OvershootInterpolator(2F));
				inoutimagebutton.startAnimation(animation);
			
		}
	}
	public static void startAnimationsOut(ViewGroup viewgroup,int durationMillis) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
				final ImageButton inoutimagebutton = (ImageButton) viewgroup
						.getChildAt(i);
				MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton.getLayoutParams();
				Animation animation = new TranslateAnimation(0F,mlp.rightMargin-xOffset, 0F,yOffset + mlp.bottomMargin);
				
				animation.setFillAfter(true);animation.setDuration(durationMillis);
				animation.setStartOffset(((viewgroup.getChildCount()-i) * 100)
						/ (-1 + viewgroup.getChildCount()));//顺序倒一下比较舒服
				animation.setInterpolator(new AnticipateInterpolator(2F));
				animation.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation arg0) {}
					@Override
					public void onAnimationRepeat(Animation arg0) {}
					@Override
					public void onAnimationEnd(Animation arg0) {
						// TODO Auto-generated method stub
						inoutimagebutton.setVisibility(8);
					}
				});
				inoutimagebutton.startAnimation(animation);
			}
		
	}
	
	public static void showToolFrag(final MapActivity mapActivity,final RelativeLayout view)
	{
		for (int i = 0; i < view.getChildCount(); i++) {
			final int viewId = i;
			
			view.getChildAt(i).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							switch (viewId)
							{
							case 1:
								AnimationsToolsUtils.startAnimationsOut(view, 300);
								mapActivity.startActivity(new Intent(mapActivity, GPSCenterActivity.class));
								break;
							case 2:
								AnimationsToolsUtils.startAnimationsOut(view, 300);
								MeasureLengthFrag measureLenFrag = new MeasureLengthFrag();
								mapActivity.mMapBottomContent.setVisibility(View.GONE);
								mapActivity.mMapRightContent.setVisibility(View.INVISIBLE);
								mapActivity.addFragment(EnumViewPos.FULL, measureLenFrag, measureLenFrag.getName(), null);
								mapActivity.addFragBackListen(measureLenFrag);
								break;
							case 3:
								AnimationsToolsUtils.startAnimationsOut(view, 300);
								MeasureAreaFrag measureAreaFrag = new MeasureAreaFrag();
								mapActivity.mMapRightContent.setVisibility(View.INVISIBLE);
								mapActivity.mMapBottomContent.setVisibility(View.GONE);
								mapActivity.addFragment(EnumViewPos.FULL, measureAreaFrag,measureAreaFrag.getName(), null);
								mapActivity.addFragBackListen(measureAreaFrag);
								break;
							case 4:
							
								if(	isOthersSrefData(mapActivity))
								{
									AnimationsToolsUtils.startAnimationsOut(view, 300);
									RecordTracksFrag recordTracksFrag = new RecordTracksFrag();
									mapActivity.mMapView.setTouchListener(null);
									mapActivity.addFragment(EnumViewPos.FULL, recordTracksFrag, recordTracksFrag.getName(), null);
								}
								
								break;
							default:
								break;
							}
						}
					});
		}
	}

	/**
	 * 判断地图空间参考系
	 * @param mapActivity
	 * @return
	 */
	public static boolean isOthersSrefData(MapActivity mapActivity)
	{
		if(mapActivity.mSRefData == null)
		{
			return false;
		}
		
		String srefName = mapActivity.mSRefData.getSRSName();
		if(srefName.equalsIgnoreCase("WGS_84_MCT") || srefName.equalsIgnoreCase("WGS_1984_Web_Mercator") || srefName.equalsIgnoreCase("WGS_1984"))
		{
			return true;
		}
		else
		{
			ToolToast.showShort("该空间参考系暂时不支持轨迹记录！！！");
			return false;
		}
	}
}