package com.zondy.mapgis.android.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.tools.zip.ZipFile;

import com.zondy.mapgis.explorer.MapApplication;

import android.content.Context;

public class FileOperation
{
	public File file;

	public FileOperation(File file)
	{
		this.file = file;
	}

	public boolean FileContainStr(String inputString)
	{
		ArrayList<String> fileStrArrayList = new ArrayList<String>();
		String[] readString = readFrFile().split("\r\n");
		for (int i = 0; i < readString.length; i++)
		{
			fileStrArrayList.add(readString[i]);
			if (fileStrArrayList.contains(inputString))
			{
				return true;
			}
		}
		return false;
	}

	public String readFrFile()
	{
		try
		{
			byte[] buff = new byte[256];
			FileInputStream fis = new FileInputStream(file);
			int len = fis.read(buff);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(buff, 0, len);
			return new String(outputStream.toByteArray());

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	// 追加到文件尾
	public void write2file(String string)
	{
		String context = string + "\r\n";
		try
		{
			FileOutputStream fOutputStream = new FileOutputStream(file, true);// 设置为true，可以追加
			fOutputStream.write(context.getBytes());
			fOutputStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void clearFile(String string)
	{
		String context = string + "\r\n";
		try
		{
			FileOutputStream fos = new FileOutputStream(file, false);
			fos.write(context.getBytes());
			fos.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 读文件的得到文件字节流
	 * 
	 * @param filePath
	 * @return
	 */
	public static byte[] readFileToBytes(String filePath)
	{
		byte[] bytes = null;
		try
		{
			BufferedInputStream bi = new BufferedInputStream(new FileInputStream(filePath));
			// 输入流转字节流
			bytes = new byte[bi.available()];
			bi.read(bytes);
			bi.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return bytes;

	}

	/**
	 * 把字节流放到本地
	 * 
	 * @param bytes
	 * @param filePath
	 * @throws IOException
	 */
	public static void writeBytesFile(byte[] bytes, String filePath) throws IOException
	{
		try
		{
			BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(filePath));
			bo.write(bytes);
			bo.flush();
			bo.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 文件输入流写入到本地
	 * 
	 * @param in
	 * @param filePath
	 */
	public static void writeInputStreamFile(InputStream in, String filePath)
	{
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		BufferedOutputStream bo;
		byte[] buffer = new byte[1024];
		int ch;
		try
		{
			while ((ch = in.read(buffer)) != -1)
			{
				bytestream.write(buffer, 0, ch);
			}
			byte[] data = bytestream.toByteArray();

			// 判断父文件夹是否被创建
			File Fout = new File(filePath);
			if (!Fout.exists())
			{
				(new File(Fout.getParent())).mkdirs();
			}

			bo = new BufferedOutputStream(new FileOutputStream(filePath));
			bo.write(data);
			bo.flush();
			bo.close();
			bytestream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 读文件的得到文件字节流
	 * 
	 * @param filePath
	 * @return
	 */
	public static void writeInFile(InputStream in, String filePath)
	{
		byte[] bytes = null;

		try
		{
			BufferedInputStream bi = new BufferedInputStream(in);
			// 输入流转字节流
			bytes = new byte[bi.available()];
			bi.read(bytes);
			bi.close();

			BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(filePath));
			bo.write(bytes);
			bo.flush();
			bo.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 删除文件，可以是文件或文件夹
	 * 
	 * @param filePath(要删除的文件名)
	 * @return 删除成功返回true，否则返回false
	 */
	public static boolean delteFile(String filePath)
	{
		File file = new File(filePath);
		if (!file.exists())
		{
			System.out.println("删除文件失败:" + filePath + "不存在！");
			return false;
		}
		else
		{
			if (file.isFile())
			{
				return file.delete();
			}
			else
			{
				return deleteDirectory(filePath);
			}
		}

	}

	/**
	 * 读输入流得到文件字节流
	 * 
	 * @param filePath
	 * @return
	 */
	public static byte[] readInputStreamToBytes(InputStream in)
	{
		byte[] bytes = null;
		try
		{
			BufferedInputStream bi = new BufferedInputStream(in);
			// 输入流转字节流
			bytes = new byte[bi.available()];
			bi.read(bytes);
			bi.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return bytes;

	}

	/**
	 * 输入流转字节流
	 * */
	public static byte[] InputStreamToByte(InputStream is) throws IOException
	{
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int ch;
		while ((ch = is.read(buffer)) != -1)
		{
			bytestream.write(buffer, 0, ch);
		}
		byte data[] = bytestream.toByteArray();
		bytestream.close();
		return data;

	}

	/**
	 * 文件按日期排序
	 * 
	 * @param flieLst 文件列表
	 * @return
	 */
	public static List<File> orderByDate(List<File> flieLst)
	{
		List<File> orderFileLst = new ArrayList<File>();
		File[] fs = new File[flieLst.size()];
		flieLst.toArray(fs);
		Arrays.sort(fs, new Comparator<File>()
		{
			public int compare(File f1, File f2)
			{
				long diff = f1.lastModified() - f2.lastModified();
				if (diff > 0)
					return 1;
				else if (diff == 0)
					return 0;
				else
					return -1;
			}

			public boolean equals(Object obj)
			{
				return true;
			}

		});
		for (int i = fs.length - 1; i > -1; i--)
		{
			// System.out.println(fs[i].getName());
			// System.out.println(new Date(fs[i].lastModified()));
			orderFileLst.add(fs[i]);
		}
		return orderFileLst;
	}

	/**
	 * 
	 * @param context 上下文
	 * @param assetName assets目录件内的一个图片文件的文件名称(支持路径)
	 * @param filePath 写入SDCARD路径
	 */
	public static void mapxFromAsset(Context context, String assetName)
	{
		try
		{
			String fileFolder = MapApplication.SystemLibraryPath + "Map";
			String filePath = MapApplication.SystemLibraryPath + "Map/" + assetName;

			InputStream localInputStream = context.getAssets().open(assetName);
			byte[] bytes = readInputStreamToBytes(localInputStream);

			if (bytes.length > 0)
			{
				File file = new File(fileFolder);
				if (!file.exists())
				{
					file.mkdirs();
				}

				writeBytesFile(bytes, filePath);
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public static void mapFromAsset(Context context, String assetName)
	{
		String outputDirectory = MapApplication.PHONE_SDCARD_PATH;
		// InputStream localInputStream;
		// try {
		// localInputStream = context.getAssets().open(assetName);
		// unzip(localInputStream, outputDirectory);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		try
		{
			InputStream localInputStream = context.getAssets().open(assetName);
			writeInFile(localInputStream, outputDirectory + "/" + assetName);
			// unZipFiles("file:///android_asset/" + assetName,
			// outputDirectory);
			if (unZipFiles(outputDirectory + "/" + assetName, outputDirectory))
			{
				deleteFile(outputDirectory + "/" + assetName);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 解压ZIP文件
	 * 
	 * @param zipFileName ZIP文件
	 * @param outputDirectory 解压目的存放路径
	 */
	public static void unzip(InputStream zipFileName, String outputDirectory)
	{
		try
		{
			BufferedOutputStream dest = null;
			ZipInputStream in = new ZipInputStream(zipFileName);
			// 获取ZipInputStream中的ZipEntry条目，一个zip文件中可能包含多个ZipEntry，
			// 当getNextEntry方法的返回值为null，则代表ZipInputStream中没有下一个ZipEntry，
			// 输入流读取完成；
			ZipEntry entry = in.getNextEntry();
			while (entry != null)
			{
				// 创建以zip包文件名为目录名的根目录
				File file = new File(outputDirectory);
				file.mkdir();
				if (entry.isDirectory())
				{
					String name = entry.getName();
					name = name.substring(0, name.length() - 1);

					file = new File(outputDirectory + File.separator + name);
					file.mkdir();

				}
				else
				{
					// 判断父文件夹是否被创建
					File Fout = new File(outputDirectory, entry.getName());
					if (!Fout.exists())
					{
						(new File(Fout.getParent())).mkdirs();
					}
					int count;
					byte data[] = new byte[2048];
					file = new File(outputDirectory + File.separator + entry.getName());
					file.createNewFile();
					FileOutputStream out = new FileOutputStream(file);
					dest = new BufferedOutputStream(out, 2048);
					while ((count = in.read(data, 0, 2048)) != -1)
					{
						dest.write(data, 0, count);
					}
					dest.flush();
					dest.close();
				}
				// 读取下一个ZipEntry
				entry = in.getNextEntry();
			}
			in.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 解压(处理中文文件名乱码的问题)
	 * 
	 * @param zipFileName
	 * @param extPlace
	 * @return
	 * @throws Exception
	 */
	public static boolean unZipFiles(String zipFileName, String extPlace)
	{
		// System.setProperty("sun.zip.encoding",
		// System.getProperty("sun.jnu.encoding"));
		try
		{
			(new File(extPlace)).mkdirs();
			File f = new File(zipFileName);
			ZipFile zipFile = new ZipFile(zipFileName, "GBK"); // 处理中文文件名乱码的问题
			if ((!f.exists()) && (f.length() <= 0))
			{
				throw new Exception("要解压的文件不存在!");
			}
			String strPath, gbkPath, strtemp;
			File tempFile = new File(extPlace);
			strPath = tempFile.getAbsolutePath();
			Enumeration<?> e = zipFile.getEntries();
			while (e.hasMoreElements())
			{
				ZipEntry zipEnt = (ZipEntry) e.nextElement();
				gbkPath = zipEnt.getName();
				if (zipEnt.isDirectory())
				{
					strtemp = strPath + File.separator + gbkPath;
					File dir = new File(strtemp);
					dir.mkdirs();
					continue;
				}
				else
				{
					// 读写文件
					InputStream is = zipFile.getInputStream((org.apache.tools.zip.ZipEntry) zipEnt);
					BufferedInputStream bis = new BufferedInputStream(is);
					gbkPath = zipEnt.getName();
					strtemp = strPath + File.separator + gbkPath;

					// 建目录
					String strsubdir = gbkPath;
					for (int i = 0; i < strsubdir.length(); i++)
					{
						if (strsubdir.substring(i, i + 1).equalsIgnoreCase("/"))
						{
							String temp = strPath + File.separator + strsubdir.substring(0, i);
							File subdir = new File(temp);
							if (!subdir.exists())
								subdir.mkdir();
						}
					}
					FileOutputStream fos = new FileOutputStream(strtemp);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
					int c;
					while ((c = bis.read()) != -1)
					{
						bos.write((byte) c);
					}
					bos.close();
					fos.close();
				}
			}
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除单个文件
	 * 
	 * @param fileName 要删除的文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String fileName)
	{
		File file = new File(fileName);
		// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
		if (file.exists() && file.isFile())
		{
			if (file.delete())
			{
				System.out.println("删除单个文件" + fileName + "成功！");
				return true;
			}
			else
			{
				System.out.println("删除单个文件" + fileName + "失败！");
				return false;
			}
		}
		else
		{
			System.out.println("删除单个文件失败：" + fileName + "不存在！");
			return false;
		}
	}

	/**
	 * 删除目录及目录下的文件
	 * 
	 * @param dir 要删除的目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String dir)
	{
		// 如果dir不以文件分隔符结尾，自动添加文件分隔符
		if (!dir.endsWith(File.separator))
			dir = dir + File.separator;
		File dirFile = new File(dir);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if ((!dirFile.exists()) || (!dirFile.isDirectory()))
		{
			System.out.println("删除目录失败：" + dir + "不存在！");
			return false;
		}
		boolean flag = true;
		// 删除文件夹中的所有文件包括子目录
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++)
		{
			// 删除子文件
			if (files[i].isFile())
			{
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
			// 删除子目录
			else if (files[i].isDirectory())
			{
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
		}
		if (!flag)
		{
			System.out.println("删除目录失败！");
			return false;
		}
		// 删除当前目录
		if (dirFile.delete())
		{
			System.out.println("删除目录" + dir + "成功！");
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * 打印文件
	 * 
	 * @param fileLst
	 */
	public static void printLst(List<File> fileLst)
	{
		System.out.println("******************");
		for (File file : fileLst)
		{
			System.out.println(file.getName());
			System.out.println(new Date(file.lastModified()));
		}
		System.out.println("================");
	}
}
