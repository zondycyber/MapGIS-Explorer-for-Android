package com.zondy.mapgis.android.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.text.TextUtils;

import com.zondy.mapgis.core.geometry.Dot;

/**
 * 静态对值做判断或操作处理
 * 
 */
public final class ValueUtil
{
	/**
	 * 判断一个值是否为空 当传入为 null 或 "" 时都认为是空
	 * 
	 * @param value 传入值
	 * @return 返回判断结果，true为是空，false为不是空
	 */
	public static boolean isEmpty(Object value)
	{
		if (value == null)
			return true;
		else if ("".equals(value.toString()))
			return true;
		else
			return false;
	}

	public static boolean isListEmpty(List<? extends Object> list)
	{
		if (list == null)
			return true;
		else if (list.size() == 0)
			return true;
		else
			return false;
	}

	public static boolean isDotEmpty(Dot dot)
	{
		if (dot == null)
		{
			return true;
		}
		else if (dot.getX() == 0 || dot.getY() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static boolean isDotEqual(Dot dotA, Dot dotB)
	{
		double aa = 10e-9;
		if (ValueUtil.isEmpty(dotA) || ValueUtil.isEmpty(dotB))
			return false;
		else if (Math.abs(dotA.getX() - dotB.getX()) < 10e-9 && Math.abs(dotA.getY() - dotB.getY()) < 10e-9)
			return true;
		else
			return false;
	}

	public static Dot[] toArrayDot(List<Dot> dotList)
	{
		if (ValueUtil.isListEmpty(dotList))
			return null;
		Dot[] dots = new Dot[dotList.size()];
		for (int i = 0; i < dotList.size(); i++)
		{
			dots[i] = dotList.get(i);
		}
		return dots;
	}

	public static boolean contain(List<Dot> dots, Dot dot)
	{
		if (dots.size() == 0)
			return false;
		else if (!ValueUtil.isDotEqual(dots.get(dots.size() - 1), dot))
			return false;
		else
			return true;
	}

	public static boolean contain(Dot[] dots, Dot dot)
	{
		if (dots == null)
			return false;
		else
		{
			for (int i = 0; i < dots.length; i++)
			{
				if (ValueUtil.isDotEqual(dot, dots[i]))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 获取系统当前时间
	 * @return
	 */
	public static String getCurrentTime()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		return dateFormat.format(date);
	}

	/**
	 * 格式化时间
	 */
	private static SimpleDateFormat sdf = null;

	public synchronized static String formatUTC(long l, String strPattern)
	{
		if (TextUtils.isEmpty(strPattern))
		{
			strPattern = "yyyy-MM-dd HH:mm:ss";
		}
		if (sdf == null)
		{
			try
			{
				sdf = new SimpleDateFormat(strPattern, Locale.CHINA);
			}
			catch (Throwable e)
			{
			}
		}
		else
		{
			sdf.applyPattern(strPattern);
		}
		if (l <= 0l)
		{
			l = System.currentTimeMillis();
		}
		return sdf == null ? "NULL" : sdf.format(l);
	}

	// 时间计数器，最多只能到99小时，如需要更大小时数需要改改方法
	public static String showTimeCount(long time)
	{
		System.out.println("time=" + time);
		if (time >= 360000000)
		{
			return "00:00:00";
		}
		String timeCount = "";
		long hourc = time / 3600000;
		String hour = "0" + hourc;
		hour = hour.substring(hour.length() - 2, hour.length());

		long minuec = (time - hourc * 3600000) / (60000);
		String minue = "0" + minuec;
		minue = minue.substring(minue.length() - 2, minue.length());

		long secc = (time - hourc * 3600000 - minuec * 60000) / 1000;
		String sec = "0" + secc;
		sec = sec.substring(sec.length() - 2, sec.length());
		if(hour.equalsIgnoreCase("00"))
		{
			timeCount = minue + ":" + sec;
		}
		else 
		{
			timeCount = hour + ":" + minue + ":" + sec;
		}
		
		System.out.println("timeCount=" + timeCount);
		return timeCount;
	}

}
