package com.zondy.mapgis.android.utils;

import java.util.Timer;
import java.util.TimerTask;

import com.zondy.mapgis.explorer.R;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

/**
 * 自定义控件，实现倒计时功能。
 * 
 * @author Admin
 * 
 */
public class DaoJiShi extends FrameLayout {

	private static final int jishi = 0x000;
	private SmartButton9 daojishi;
	private Handler handler;
	private Timer timer;
	private int allTime = 50;
	private DaoJiShiCallBack daojishicallback;

	public DaoJiShi(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.daojishi, this);
		initView();
		initHandler();
		initListerner();
	}

	public DaoJiShi(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.daojishi, this);
		initView();
		initHandler();
		initListerner();
	}

	public DaoJiShi(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.daojishi, this);
		initView();
		initHandler();
		initListerner();
	}

	public void initView() {
		daojishi = (SmartButton9) findViewById(R.id.daojishi_btn);
	}

	public void initListerner() {
		daojishi.setMyOnClick(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (daojishicallback != null) {
					if (daojishicallback.Start()) {
						// 手机号验证通过了，那么继续下面的操作。
					} else {
						// 手机号验证未通过，直接返回了，不执行下面的操作。
						return;
					}
				} else {
					// 回调接口为空，有问题，直接返回了，不执行下面的操作。
					return;
				}
				daojishi.setClickable(false);
				// 按钮按下时创建一个Timer定时器
				timer = new Timer();
				// 创建一个TimerTask
				// TimerTask是个抽象类,实现了Runnable接口，所以TimerTask就是一个子线程
				TimerTask timerTask = new TimerTask() {
					// 倒数allTime秒,默认50秒。
					int i = allTime;

					@Override
					public void run() {
						// Log.d("debug", "run方法所在的线程："
						// + Thread.currentThread().getName());
						// 定义一个消息传过去
						Message msg = new Message();
						msg.what = i--;
						handler.sendMessage(msg);
						if (i < 0) {
							cancel();
						}
					}
				};
				// 定义计划任务，根据参数的不同可以完成以下种类的工作：
				// １．schedule(TimerTask task, Date when)　ー＞　在固定时间执行某任务
				// ２．schedule(TimerTask task, Date when, long
				// period)　ー＞　在固定时间开始重复执行某任务，重复时间间隔可控
				// ３．schedule(TimerTask task, long delay)　ー＞　在延迟多久后执行某任务
				// ４．schedule(TimerTask task, long delay, long
				// period)　ー＞　在延迟多久后重复执行某任务，重复时间间隔可控
				timer.schedule(timerTask, 1, 1000);// 3秒后开始倒计时，倒计时间隔为1秒
			}
		});
	}

	public void initHandler() {
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				daojishi.setText(msg.what + "秒后\n重新获取");
				if (daojishicallback != null) {
					daojishicallback.numChanged(msg.what);
				}
				if (msg.what == 0) {
					daojishi.setText("获取验证码");
					daojishi.setClickable(true);
					if (daojishicallback != null) {
						daojishicallback.End();
					}
				}
			}
		};
	}

	public void stop() {
		timer.cancel();
		daojishi.setText("获取验证码");
		daojishi.setClickable(true);
	}

	public void start() {
		daojishi.performClick();
	}

	public void setDaojishicallback(DaoJiShiCallBack daojishicallback) {
		this.daojishicallback = daojishicallback;
	}

	public void setAllTime(int allTime) {
		this.allTime = allTime;
	}

	/**
	 * 倒计时控件回调外部代码的接口。
	 * 
	 * @author Admin
	 * 
	 */
	public interface DaoJiShiCallBack {

		/**
		 * 点击按钮后，开始计时前调用的方法。
		 * 
		 * @return 返回true会开始计时，false会退出计时。
		 */
		public boolean Start();

		/**
		 * 结束啦。
		 */
		public void End();

		/**
		 * 数字发生变化了。
		 * 
		 * @param num
		 * @return
		 */
		public void numChanged(int num);

	}

}
