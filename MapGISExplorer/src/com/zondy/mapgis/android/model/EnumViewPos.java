package com.zondy.mapgis.android.model;

/**
 * 在视图中的位置
 * @author fjl
 */
public enum EnumViewPos {
	/**
	 * 全屏
	 */
	FULL,
	/**
	 * 左部的顶部
	 */
	LEFTTOP,
	/**
	 * 左部的中心
	 */
	LEFTCENTER,
	/**
	 * 左部全屏
	 */
	LEFTFULL,
	/**
	 * 左部底部
	 */
	LEFTBOTTOM,
	/**
	 * 右部上部
	 */
	RIGHTTOP,
	/**
	 * 右部中心
	 */
	RIGHTCENTER,
	/**
	 * 右部全屏
	 */
	RIGHTFULL,
	/**
	 * 右部底部
	 */
	RIGHTBOTTOM,
	/**
	 * 地图上方
	 */
	MAPHEAD,
	/**
	 * 地图上方部分，即在地图范围内
	 */
	TOP,
	/**
	 * 底部
	 */
	BOTTOM,
	/**
	 * 底部左侧
	 */
	BOTTOM_LEFT,
	/**
	 *
	 */
	BOTTOM_RIGHT,
	/**
	 *
	 */
	JXEDIT
}
