package com.zondy.mapgis.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.zondy.mapgis.android.view.StretchPanel;
import com.zondy.mapgis.explorer.R;

/**
 * @content 定位数据转换工具
 * @author fjl 2016-12-6 上午11:33:20
 */

public class LocationTransActivity1 extends Activity
{
	// private String TAG = this.getClass().getSimpleName();
	private StretchPanel mStretchPanel;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location_trans_layout);

		initView();
	}

	/**
	 * 初始化窗体
	 */
	private void initView()
	{
		mStretchPanel = (StretchPanel) findViewById(R.id.stretchPanel);
		final View contentView = View.inflate(this, R.layout.view_stretch_content, null);
		final View stretchView = View.inflate(this, R.layout.view_stretch_threeparams, null);
		mStretchPanel.setContentView(contentView);
		mStretchPanel.setStretchView(stretchView);
		mStretchPanel.openStretchView();

		mStretchPanel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (mStretchPanel.isStretchViewOpened())
				{
					mStretchPanel.closeStretchView();
				}
				else
				{
					mStretchPanel.openStretchView();
				}
			}
		});

	}

}
