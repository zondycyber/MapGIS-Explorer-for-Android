package com.zondy.mapgis.android.activity;


import com.zondy.mapgis.android.base.BaseActivity;
import com.zondy.mapgis.android.model.AndroidShare;
import com.zondy.mapgis.explorer.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class AccountActivity extends BaseActivity implements OnClickListener
{
	private TextView mTile = null;
	private ImageView mTileBack = null; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		initView();
	}
	
	/**
	 * 初始化窗体
	 */
	public void initView()
	{
		setContentView(R.layout.mimepage_layout);
		
		mTile = (TextView) findViewById(R.id.title_text);
		mTile.setText("更多");
		findViewById(R.id.mime_layout_feedback).setOnClickListener(this);
		findViewById(R.id.share_layout).setOnClickListener(this);
		findViewById(R.id.tracks_layout).setOnClickListener(this);
		
		mTileBack = (ImageView) findViewById(R.id.title_back);
		mTileBack.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.mime_layout_feedback:
			startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
			break;

		case R.id.share_layout:
			AndroidShare share = new AndroidShare(AccountActivity.this, "heiheihei", "http://ww1.sinaimg.cn/large/bca0b9cfjw1dz9h6ufo8dj.jpg");
			share.show();
			break;
		case R.id.tracks_layout:
			startActivityForResult(new Intent(getApplicationContext(), MyTracksActivity.class), 1);
//			startActivity(new Intent(getApplicationContext(), MyTracksActivity.class));
			finish();
			break;
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}
	
	


}
