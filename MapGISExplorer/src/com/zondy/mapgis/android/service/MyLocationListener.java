package com.zondy.mapgis.android.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

/**
 *  实时位置回调监听
 * @author fjl 2016-12-15 上午11:06:44
 */
public class MyLocationListener  implements BDLocationListener
{
	private String  TAG = this.getClass().getSimpleName(); 
	public Handler locHander;
	
	public MyLocationListener(Handler hander)
	{
		this.locHander = hander;
	}
	
	@Override
	public void onReceiveLocation(BDLocation location)
	{
		Message locMsg = locHander.obtainMessage();
		Bundle locData = new Bundle();

		if (null != location && location.getLocType() != BDLocation.TypeServerError)
		{
			locData.putParcelable("loc", location);
			locMsg.setData(locData);
			locHander.sendMessage(locMsg);
			
			StringBuffer sb = new StringBuffer(256);
//			sb.append("time : ");
//			sb.append(location.getTime());
			sb.append("\nlatitude : ");// 纬度
			sb.append(location.getLatitude());
			sb.append("\nlontitude : ");// 经度
			sb.append(location.getLongitude());
//			sb.append("\nDistrict : ");// 区
//			sb.append(location.getDistrict());
//			sb.append("\nStreet : ");// 街道
//			sb.append(location.getStreet());
			sb.append("\naddr : ");// 地址信息
			sb.append(location.getAddrStr());
//			sb.append("\nlocationdescribe: ");
//			sb.append(location.getLocationDescribe());// 位置语义化信息
//			sb.append("\n角度 : ");// 方向角
//			sb.append(location.getDirection());
			Log.d(TAG, sb.toString());
		}
		else
		{
			Log.e(TAG, "定位失败！！");
			Log.e(TAG, "" + location.getLocType());
		}
	
	}
	
	public Handler getHander()
	{
		return locHander;
	}

	public void setHander(Handler hander)
	{
		this.locHander = hander;
	}


}
