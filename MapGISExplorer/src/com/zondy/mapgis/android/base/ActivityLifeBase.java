package com.zondy.mapgis.android.base;

import com.zondy.mapgis.android.map.dao.IActivityLife;


/**
 * Activity生命周期事件基类
 * @author yangsheng
 * @date 2015年1月28日
 */
public class ActivityLifeBase implements IActivityLife {

	@Override
	public void onResume() {
	}

	@Override
	public void onCreate() {
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onRestart() {
	}

	@Override
	public void onPause() {
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onDestroy() {
	}

}
