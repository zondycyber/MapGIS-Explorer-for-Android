package com.zondy.mapgis.android.gpx.extensions;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.zondy.mapgis.android.gpx.bean.GPX;
import com.zondy.mapgis.android.gpx.bean.Route;
import com.zondy.mapgis.android.gpx.bean.Track;
import com.zondy.mapgis.android.gpx.bean.Waypoint;

public class DummyExtensionParser
  implements IExtensionParser
{
  public String getId()
  {
    return "Basic Extension Parser";
  }

  public Object parseWaypointExtension(Node paramNode)
  {
    return "Parsed Waypoint data";
  }

  public Object parseTrackExtension(Node paramNode)
  {
    return "Parsed Track data";
  }

  public Object parseGPXExtension(Node paramNode)
  {
    return "Parsed GPX data";
  }

  public Object parseRouteExtension(Node paramNode)
  {
    return "Parsed Route data";
  }

  public void writeGPXExtensionData(Node paramNode, GPX paramGPX, Document paramDocument)
  {
  }

  public void writeWaypointExtensionData(Node paramNode, Waypoint paramWaypoint, Document paramDocument)
  {
    Element localElement = paramDocument.createElement("mySampleExtension");
    localElement.setNodeValue("mySampleWaypointValue");
    paramNode.appendChild(localElement);
  }

  public void writeTrackExtensionData(Node paramNode, Track paramTrack, Document paramDocument)
  {
    Element localElement = paramDocument.createElement("mySampleExtension");
    localElement.setNodeValue("mySampleTrackValue");
    paramNode.appendChild(localElement);
  }

  public void writeRouteExtensionData(Node paramNode, Route paramRoute, Document paramDocument)
  {
    Element localElement = paramDocument.createElement("mySampleExtension");
    localElement.setNodeValue("mySampleRouteValue");
    paramNode.appendChild(localElement);
  }
}