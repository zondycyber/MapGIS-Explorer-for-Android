package com.zondy.mapgis.android.map.dao;

/**
 * 简单的接口，用于处理返回键
 * @author hc
 *
 */
public interface ICallBackGoBack {

	/**
	 * 
	 * @param str
	 * @return 1/0/-1 -->保存或不保存或直接点击返回键隐藏掉Fragment/不返回/主动隐藏掉Fragment
	 */
	public int onCall(String str);
}
