package com.zondy.mapgis.android.map.dao;

import java.util.ArrayList;
import java.util.List;

import com.zondy.mapgis.core.srs.SRefData;
import com.zondy.mapgis.map.model.DRect;


/**
 * 地图基础接口，把地图抽取接口出来，可以方便地以相同方法处理 mapgis 和 arcgis 的地图类
 * @author yangsheng
 * @date 2014年11月6日
 */
public interface IBaseMap {

	/**
	 * 地图名
	 * @return
	 */
	public String getName();

	public double getMaximalScale();

	public double getMinimumScale();

	/**
	 * 如果图层分组了，则返回所有组图层，如果没有分组，则返回所有图层
	 * @return
	 */
	public ArrayList<IBaseLayer> getLayers();

	/**
	 * 取所有图层，不分组。
	 * @return
	 */
	public ArrayList<IBaseLayer> getAllLayers();

	/**
	 * 取所有组图层
	 * @return
	 */
	public ArrayList<IBaseLayer> getGroupLayers();

	/**
	 * 取所有子图层
	 * @return
	 */
	public ArrayList<ArrayList<IBaseLayer>> getChildLayers();

	/**
	 * 取地图中所有图层名，不分组
	 * @return
	 */
	public List<String> getLayerNamesInMap();

	/**
	 * 取地图中所有图层名，不分组
	 * @param useAlias 是否使用别名
	 * @return
	 */
	public List<String> getLayerNamesInMap(boolean useAlias);

	/**
	 * 取地图中所有矢量图层名，不分组
	 * @return
	 */
	public List<String> getVctLyrNamesInMap();

	/**
	 * 取地图中所有矢量图层名，不分组
	 * @param useAlias 是否使用别名
	 * @return
	 */
	public List<String> getVctLyrNamesInMap(boolean useAlias);

	/**
	 * 取地图中所有矢量图层，不分组
	 * @return
	 */
	public List<IBaseLayer> getVctLayers();

	/**
	 * 查找所有子图层中以strNameEnd结尾的Layer
	 * @param layers
	 * @param strNameEnd
	 * @return
	 */
	public List<IBaseLayer> getLayersByNameEnd(String strNameEnd);

	/**
	 * 查找所有子图层中以strNameEnd结尾的Layer
	 * @param layers
	 * @param strNameEnd
	 * @param useAlias
	 * @return
	 */
	public List<IBaseLayer> getLayersByNameEnd(String strNameEnd, boolean useAlias);

	/**
	 * 查找所有子图层中包含strNameEnd的Layer
	 * @param layers
	 * @param strNameEnd
	 * @return
	 */
	public List<IBaseLayer> getLayersByNameSub(String strNameSub);

	/**
	 * 查找所有子图层中包含strNameEnd的Layer
	 * @param layers
	 * @param strNameEnd
	 * @param useAlias
	 * @return
	 */
	public List<IBaseLayer> getLayersByNameSub(String strNameSub, boolean useAlias);

	/**
	 * 获取第一个矢量图层，只考虑第一层下的矢量图层
	 * @return
	 */
	public IBaseLayer getFirstLayer();

	/**
	 * 根据图层名取图层，组图层和子图层均可查
	 * @param strName
	 * @return
	 */
	public IBaseLayer getLayerByName(String strName);

	/**
	 * 根据图层名取图层，组图层和子图层均可查。<br>
	 * 如果使用了图层别名，而查找所有图层后没有找到匹配图层，则有可能参数strName是某一个图层
	 * 的 名称 但是该图层已经设置了别名。这种情况下，仍然将返回该图层。
	 * @param strName
	 * @param useAlias 是否使用图层别名
	 * @return
	 */
	public IBaseLayer getLayerByName(String strName, boolean useAlias);

	/**
	 * 地图的图层数
	 * @return
	 */
	public int getLayerCount();

	/**
	 * 是否分组
	 * @return
	 */
	public boolean isGrouped();

	/**
	 * 地图的包络
	 * @return
	 */
	public DRect getRange();

	/**
	 * 获取地图下所有图层的基本信息，包含了组图层下的子图层的信息
	 * @return
	 */
//	public List<LayerInfo> getLayerInfosEx();

	/**
	 * 设置地图各图层的一些附加信息
	 * @see #setLayerInfoMins(List)
	 * @param mi
	 * @return
	 */
//	public boolean setMapInfo(MapInfo mi);

	/**
	 * 设置地图各图层的一些附加信息
	 * @param limListIn 图层简要信息，要求列表大小、图层顺序、图层名称与本地图图层完全一致
	 * @return limListIn不符合要求，则操作失败，返回false，否则返回true。
	 */
//	public boolean setLayerInfoMins(List<LayerInfoMin> limListIn);

	/**
	 * 取地图下的图层简要信息<br>
	 * @return
	 */
//	public List<LayerInfoMin> getLayerInfoMins();

	/**
	 * 取坐标系
	 * @return
	 */
	public SRefData getSrefData();

	public static enum MapType {
		MapGIS, ArcGIS;
	}
}
