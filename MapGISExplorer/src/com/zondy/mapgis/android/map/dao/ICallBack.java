package com.zondy.mapgis.android.map.dao;

/**
 * 简单的接口<br>
 * @author yangsheng
 */
public interface ICallBack {
	public void onCall(String str);
}
