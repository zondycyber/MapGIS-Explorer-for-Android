package com.zondy.mapgis.android.map.dao;

/**
 * 返回接口
 * @author yangsheng
 */
public interface IGoBack {
	/**
	 * 返回操作
	 * @return 是否正确处理完毕
	 */
	public boolean goback();
}
