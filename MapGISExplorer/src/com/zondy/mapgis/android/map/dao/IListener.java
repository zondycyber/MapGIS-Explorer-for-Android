package com.zondy.mapgis.android.map.dao;

/**
 * 回调，泛型
 * @author yangsheng
 * @date 2014年11月7日
 * @param <T>
 */
public interface IListener<T> {
	/**
	 * 回调方法
	 * @param t
	 */
	public void onCall(T t);
	
}
