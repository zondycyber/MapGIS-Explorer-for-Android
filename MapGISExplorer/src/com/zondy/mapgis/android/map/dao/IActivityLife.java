package com.zondy.mapgis.android.map.dao;

import android.app.Activity;

/**
 * @content Activity的生命周期事件接口，如果只需要部分生命周期事件，可使用 {@link ActivityLifeBase}
 * @author admin 2016-7-22 上午10:32:26
 */
public interface IActivityLife {
	/**
	 * @see Activity#onCreate()
	 */
	public void onCreate();
	/**
	 * @see Activity#onStart()
	 */
	public void onStart();
	/**
	 * @see Activity#onRestart()
	 */
	public void onRestart();
	/**
	 * @see Activity#onResume()
	 */
	public void onResume();
	/**
	 * @see Activity#onPause()
	 */
	public void onPause();
	/**
	 * @see Activity#onStop()
	 */
	public void onStop();
	/**
	 * @see Activity#onDestroy()
	 */
	public void onDestroy();
}
