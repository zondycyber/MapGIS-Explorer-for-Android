package com.zondy.mapgis.android.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.core.map.MapServerType;
import com.zondy.mapgis.core.object.Enumeration;
import com.zondy.mapgis.explorer.R;

public class TestActivity extends Activity implements OnClickListener
{

	private String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);

		findViewById(R.id.test).setOnClickListener(this);
		// 枚举类型测试
		Enumeration[] testEnum = MapServerType.getEnums(MapServerType.class);
		for (Enumeration e : testEnum)
		{
			Log.d(TAG, "testEnum:" + e.name());
			Log.d(TAG, "testEnum:" + e.value());
		}
		String[] strs = MapServerType.getNames(MapServerType.class);
		for (String s : strs)
		{
			Log.d(TAG, "getNames:" + s.toString());
		}
		String str = MapServerType.getNameByValue(MapServerType.BaiduMap.getClass(), 600);
		Log.d(TAG, "str:" + str.toString());
		Enumeration baiduenum = MapServerType.parse(MapServerType.class, str);
		Log.d(TAG, "baiduenum:" + baiduenum.name());
		Enumeration gooleenum = MapServerType.parse(MapServerType.class, 104);
		Log.d(TAG, "gooleenum:" + gooleenum.name());
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.test:
			// MapApplication.getApp().setMapActivityClass(MapActivity.class);
			// MapApplication.getApp().openActivity(MainActivity.this,
			// MapActivity.class, null, false);
			startActivity(new Intent(getApplicationContext(), MapActivity.class));
			break;

		default:
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
