package com.zondy.mapgis.android.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.zondy.dizhi.server.util.ServerUtil;
import com.zondy.mapgis.android.utils.FileOperation;
import com.zondy.mapgis.explorer.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class FjlTestActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_test);
		
		ServerUtil.UpdateVersion1(this);
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
//				urlTest();
				
//				registerTest();
				
			}
		}).start();
		
	}
	
	
	private void registerTest()
	{
		HttpURLConnection connection = null;
		byte[] data = null;
		String strResult = null;
		
		URL postUrl;
		try
		{
			postUrl = new URL("http://www.smaryun.com/ucenter/user.php?a=sendRegCode");
			connection = (HttpURLConnection) postUrl.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.connect();
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			String content = String.format("user=%s&remember=false&format=json", "15307145167");
			out.writeBytes(content);
			out.flush();
			out.close();
			
			// 判断请求是否成功
			if (connection.getResponseCode() == 200)
			{
				// 获取返回的数据
				data = FileOperation.InputStreamToByte(connection.getInputStream());
				// 关闭连接
				connection.disconnect();
			}
			strResult = getBytesToString(data);
			Log.e("strResult:", strResult);
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	
		
	}
	
	
	private void urlTest()
	{
		BufferedReader reader = null;
		HttpURLConnection connection = null;
		String strResult = null;
		String strID = "";

//		obj.success = false;
		try
		{
			URL postUrl = new URL("http://www.smaryun.com/ucenter/user.php?a=login");
			connection = (HttpURLConnection) postUrl.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.connect();
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			String content = String.format("username=%s&password=%s&remember=false&format=json", "850991007", "fjl0328666fjl");
			out.writeBytes(content);
			out.flush();
			out.close();
			// 根据ResponseCode判断连接是否成功
			int responseCode = connection.getResponseCode();
			if (responseCode != 200)
			{
//				obj.strErrorMsg = responseCode + "：连接服务器异常!";
			}
			else
			{
//				byte[] data = FileOperation.InputStreamToByte(connection.getInputStream());
//				String tets = getBytesToString(data);
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				int i = 0;
				while ((line = reader.readLine()) != null)
				{
					if (i == 0)
					{
						strResult = line;
					}
					else
					{
						strResult += "\r\n" + line;
					}
					i++;
				}
				reader.close();
			}
		}
		catch (Exception e)
		{
//			obj.strErrorMsg = "网络连接出现异常!";
		}
		finally
		{
			connection.disconnect();
			try
			{
				if (reader != null)
				{
					reader.close();
				}
			}
			catch (IOException ex)
			{

			}
		}

		if (strResult == null)
		{
//			obj.strErrorMsg = "网络连接出现异常!";
		}
		else
		{

			try
			{
				JSONTokener jsonParser = new JSONTokener(strResult);
				JSONObject msg = (JSONObject) jsonParser.nextValue();
				int errorFlag = msg.getInt("error");

				if (errorFlag == 0)
				{
					// 成功
//					obj.success = true;
					strID = msg.getString("ecs_id");
				}
				else
				{
//					obj.success = false;
//					obj.strErrorMsg = msg.getString("content");
//					obj.lastCount = msg.getInt("last_count");
				}
			}
			catch (JSONException e)
			{
//				obj.strErrorMsg = "网络连接出现异常!";
			}
		}

	}
	
	/**
	 * 获取URL的二进制
	 * 
	 * @param urlPath
	 * @return byte[]
	 */
	public byte[] getHtttpUrlBytes(String urlPath)
	{

		byte[] data = null;
		try
		{
			URL url = new URL(urlPath);
			// 打开一个HttpURLConnection连接
			HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
			// 设置连接超时时间
			urlConn.setConnectTimeout(30 * 1000);
			// 开始连接
			urlConn.connect();
			int code = urlConn.getResponseCode();
			// 判断请求是否成功
			if (urlConn.getResponseCode() == 200)
			{
				// 获取返回的数据
				data = FileOperation.InputStreamToByte(urlConn.getInputStream());
				// 关闭连接
				urlConn.disconnect();
			}
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return data;
	}
	
	/**
	 * 获取二进制转字符
	 * 
	 * @param bytes
	 * @return
	 */
	public static String getBytesToString(byte[] bytes)
	{
		String result = null;

		try
		{
//			JsonReader reader = new JsonReader(new StringReader(result));
//			reader.setLenient(true);
			result = new String(bytes, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return result;
	}

}
