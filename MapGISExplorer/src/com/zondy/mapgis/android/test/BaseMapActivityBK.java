package com.zondy.mapgis.android.test;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.zondy.mapgis.android.base.BaseActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.map.dao.IActivityLife;
import com.zondy.mapgis.android.map.dao.ICallBack;
import com.zondy.mapgis.android.model.EnumViewPos;
import com.zondy.mapgis.map.model.DRect;

/**
 * 基础地图{@link Activity}。继承自{@link BaseActivity}<br>
 * 定义了一组接口，和部分接口的默认实现，不包含任何界面相关操作。
 * 其子类应当实现所有接口。
 * 功能与界面关联起。
 * @author yangsheng
 */
public abstract class BaseMapActivityBK extends BaseActivity{

	protected static BaseMapActivityBK activity;
//	protected IBaseMapView mapView;
//	protected LandMapView landMapView;
//	protected ArcMapView arcMapView;
	// protected static ViewMode viewMode = ViewMode.GatherCenter;
	protected static DRect rect;
	protected boolean isFirstLoad;
	
	protected List<IActivityLife> lstLife = new ArrayList<IActivityLife>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;

		isFirstLoad = true;
		for(IActivityLife life : lstLife){
			life.onCreate();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		isFirstLoad = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		for(IActivityLife life : lstLife){
			life.onResume();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		for(IActivityLife life : lstLife){
			life.onDestroy();
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		for(IActivityLife life : lstLife){
			life.onStart();
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		for(IActivityLife life : lstLife){
			life.onStop();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		for(IActivityLife life : lstLife){
			life.onPause();
		}
	}

	/**
	 * 继承该方法，然后 在onCreate()方法中调用。
	 */
	protected abstract void initCtrl();

	/**
	 * 加载默认地图<br>
	 */
	public abstract boolean loadMap();

	/**
	 * 切换地图
	 * @param strMapName
	 */
	public abstract boolean changeMap(String strMapName);

	/**
	 * 切换地图，简化版
	 * @param strMapName
	 * @return
	 */
	public abstract boolean changeMapSub(String strMapName);
	
	/**
	 * 在地图左边添加快捷方式
	 * @param bgResId
	 * @param callback
	 * @return
	 */
	public abstract ImageView addLink(final int bgResId, final ICallBack callback);



	/**
	 * 移除菜单列表指定位置上的菜单
	 * @param index
	 * @return
	 */
	public abstract int removeMenuOpers(int index);

	/**
	 * 移除菜单列表中指定名称的菜单
	 * @param strMenuName
	 * @return
	 */
	public abstract int removeMenuOpers(String strMenuName);

	/**
	 * 移除图层管理菜单
	 */
	public abstract void removeMenuOperLayer();

	/**
	 * 移除地图管理菜单
	 */
	public abstract void removeMenuOperMap();

	/**
	 * 添加图层管理菜单
	 */
	public abstract void addMenuOperLayer();

	/**
	 * 添加地图管理菜单
	 */
	public abstract void addMenuOperMap();

	/**
	 * 添加Fragment
	 * @param enumPos Fragment的位置
	 * @param frag Fragment
	 * @param strTag Fragment的名称
	 * @param callback 可自定义的接口
	 * @return
	 */
	public abstract void addFragment(EnumViewPos enumPos, BaseFragment frag, String strTag,
									 ICallBack callback);

	public abstract void addMapFragment(EnumViewPos enumPos, BaseFragment frag, String strTag,
									 ICallBack callback);

//	/**
//	 * 移除Fragment
//	 * @param strTag Fragment的名称
//	 * @param callback 可自定义的接口
//	 * @return
//	 */
//	public abstract void removeFragment(String strTag, ICallBack callBack);
//
//	/**
//	 * 隐藏Fragment
//	 * @param strTag Fragment的名称
//	 * @param callback 可自定义的接口
//	 */
//	public abstract void hideFragment(String strTag, ICallBack callback);
//
//	/**
//	 * 显示Fragment
//	 * @param strTag Fragment的名称
//	 * @param callback 可自定义的接口
//	 */
//	public abstract void showFragment(String strTag, ICallBack callback);

	/**
	 * 刷新一下各种绘图图元的可见性<br>
	 * 如坐标等
	 */
	public abstract void ctrlGraphicVisibility();

	/**
	 * 刷新一下在指定放大级数上的各种绘图图元的可见性
	 * @param zoom
	 */
	public abstract void ctrlGraphicVisibility(float zoom);

	
	/**
	 * 首次加载地图后地图放大到 rect 范围
	 * @param rect 空间范围
	 */
	public static void zoomToRangeAfterMapFirstLoad(DRect rectTo) {
		rect = rectTo;
	}

	/**
	 * 取首次加载地图后放大的范围
	 */
	public static DRect getZoomRangeFirstLoad(){
		return rect;
	}

	/**
	 * 开始定位
	 */
	public abstract void startLocate();

	/**
	 * 是否正在定位
	 * @return
	 */
	public abstract boolean isLocate();


	public abstract void setChangeMapCallBack(ICallBack callBack);

	public abstract boolean isTextGraphicVisible();
	
	
//	/**
//	 * 按返回键时调用该方法。
//	 * @return
//	 */
//	public abstract boolean goback();
//
//	
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			boolean handled = goback();
//			if (handled) {
//				return true;
//			}
//		}
//
//		return super.onKeyDown(keyCode, event);
//	}

	/**
	 * 是否第一次创建
	 * @return
	 */
	public boolean isFirstLoad() {
		return isFirstLoad;
	}

	/**
	 * 获取地图视图
	 * @return
	 */
//	public IBaseMapView getMapView() {
//		return this.mapView;
//	}
	
	/**
	 * 取MapGIS版本的地图视图。
	 * @return
	 */
//	public LandMapView getLandMapView(){
//		if(mapView != null && landMapView == null && mapView instanceof LandMapView){
//			landMapView = (LandMapView)mapView;
//		}
//		return landMapView;
//	}
	



	/**
	 * 清除地图视图上所有交互事件
	 */
	public void clearAllMapListener() {
		this.clearMapClickListener();
		this.clearMapLongClickListener();
		this.clearMapLandTouchListener();
//		this.mapView.setIntercept(false);
	}

	/**
	 * 清除地图上的点击事件
	 */
	public void clearMapClickListener() {
//		this.mapView.setOnClickListener((IBaseMapView.OnClickListener) null);
	}

	/**
	 * 清除地图上的长按事件
	 */
	public void clearMapLongClickListener() {
//		this.mapView.setOnLongClickListener((IBaseMapView.OnLongClickListener) null);
	}

	/**
	 * 清除地图上的触摸事件
	 */
	public void clearMapLandTouchListener() {
//		this.mapView.setOnLandTouchListener((IBaseMapView.OnTouchListener) null);
	}

	/**
	 * 设置地图的点击事件
	 * @param oncliCk
	 */
//	public void setClickListener(IBaseMapView.OnClickListener oncliCk) {
//		if (mapView != null) {
//			mapView.setOnClickListener(oncliCk);
//		} else {
//			System.out.println("BaseMapActivity.setClickListener失败，mapView == null");
//		}
//	}

	/**
	 * 设置地图的长按事件
	 * @param onLongClick
	 */
//	public void setLongClickListener(IBaseMapView.OnLongClickListener onLongClick) {
//		if (mapView != null) {
//			mapView.setOnLongClickListener(onLongClick);
//		} else {
//			System.out.println("BaseMapActivity.setLongClickListener失败，mapView == null");
//		}
//	}

	/**
	 * 设置地图的触摸事件，一般是用在移动点时，在长按事件触发后，设置触摸事件。
	 * @param onTouch
	 */
//	public void setTouchListener(IBaseMapView.OnTouchListener onTouch) {
//		if (mapView != null) {
//			mapView.setOnLandTouchListener(onTouch);
//		} else {
//			System.out.println("BaseMapActivity.setTouchListener失败，mapView == null");
//		}
//	}

	/**
	 * 放大镜，ArcGIS下没有放大镜
	 * @param x
	 * @param y
	 */

	/**
	 * 隐藏放大镜，ArcGIS下没有放大镜
	 */

	/**
	 * 取地图Activity实例
	 * @return
	 */
	public static BaseMapActivityBK getInstance() {
		return activity;
	}

	/**
	 * 获取查询是最小容差
	 * @return
	 */
//	public double getSelMinEPS() {

//		float tolerance = 35;
//		double mindistance = mapView.getCurResolution() * tolerance;
//		return mindistance;
//	}

	/**
	 * 注册生命周期监听事件
	 * @param life
	 */
	public void registerLife(IActivityLife life){
		this.lstLife.add(life);
	}
	
	/**
	 * 取消注册生命周期监听事件
	 * @param life
	 */
	public void unRegisterLife(IActivityLife life){
		this.lstLife.remove(life);
	}
}
