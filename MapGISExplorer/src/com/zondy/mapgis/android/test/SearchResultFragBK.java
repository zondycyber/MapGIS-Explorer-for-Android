//package com.zondy.mapgis.android.test;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.zondy.mapgis.android.activity.MapActivity;
//import com.zondy.mapgis.android.base.BaseFragment;
//import com.zondy.mapgis.android.frag.FeatureDetailFrag;
//import com.zondy.mapgis.android.model.EnumViewPos;
//import com.zondy.mapgis.core.attr.Field;
//import com.zondy.mapgis.core.attr.Fields;
//import com.zondy.mapgis.core.attr.Record;
//import com.zondy.mapgis.core.featureservice.Feature;
//import com.zondy.mapgis.core.featureservice.FeaturePagedResult;
//import com.zondy.smartmobileapp.MapApplication;
//import com.zondy.mapgis.explorer.R;
//
//public class SearchResultFragBK extends BaseFragment implements OnClickListener
//{
//
//	private String TAG = this.getClass().getSimpleName();
//	private MapActivity mMapActivity = null;
//	private FeaturePagedResult mFeaturePageResult = null;
//
//	private ListView mListView = null;
//	private TextView mSearchCountTv = null;
//	private TextView mTitleTxt = null;
//	private SearchResultAdapter mSearchResultAdapter = null;
//
//	// 当前页数
//	private int mCurPageIndex = 1;
//
//	@Override
//	public int bindLayout()
//	{
//		return R.layout.bottom_searchresult_view;
//	}
//
//	@Override
//	public void initView(View view)
//	{
//		mListView = (ListView) view.findViewById(R.id.search_result_lst);
//		mSearchCountTv = (TextView) view.findViewById(R.id.search_count_tv);
//		mTitleTxt = (TextView) view.findViewById(R.id.title_text);
//		mTitleTxt.setText("查询结果");
//
//		view.findViewById(R.id.title_back).setOnClickListener(this);
//
//		Bundle data = getArguments();
//		mFeaturePageResult = (FeaturePagedResult) data.get("FeaturePagedResult");
//
//	}
//
//	@Override
//	public void doBusiness(Context mContext)
//	{
//		mMapActivity = MapApplication.getApp().getMapActivity();
//
//		// 查询结果
//		int totalCount = mFeaturePageResult.getTotalFeatureCount();
//		mSearchCountTv.setText(totalCount + "条结果");
//
//		mSearchResultAdapter = new SearchResultAdapter(getContext());
//		mSearchResultAdapter.setFeatueLst(loadPageData(mCurPageIndex));
//		mListView.setAdapter(mSearchResultAdapter);
//	}
//
//	@Override
//	public void onClick(View v)
//	{
//		switch (v.getId())
//		{
//		case R.id.title_back:
//			mMapActivity.removeFragment(TAG, null);
//			break;
//
//		default:
//			break;
//		}
//
//	}
//
//	/**
//	 * 加载一页数据
//	 * 
//	 * @param pageNum
//	 */
//	public List<Feature> loadPageData(int pageNum)
//	{
//		List<Feature> featureList = new ArrayList<Feature>();
//		featureList = mFeaturePageResult.getPage(pageNum);
//		//test
//				for (int k = 0; k < featureList.size(); k++)
//				{
//					Feature feature = featureList.get(k);
//					// 获取要素属性
//					Record record = feature.getAtt();
//					// 获取要素字段结构
//					Fields fields = feature.getFields();
//					// 获取字段数目
//					short fieldCount = fields.getFieldCount();
//					for (short j = 0; j < fieldCount; j++)
//					{
//						Field field = fields.getField(j);
//						// 获取字段名称
//						String strFieldName = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue = record.getFldVal(strFieldName);
//						Log.d(TAG, "strFieldName---" + k + ":" + strFieldName);
//						Log.d(TAG, "fieldValue===" + k + ":" + fieldValue);
//					}
//
//				}
//		return featureList;
//
//	}
//
//	class SearchResultAdapter extends BaseAdapter
//	{
//		private Context context;
//		private LayoutInflater inflater = null;
//		private List<Feature> featureLst = new ArrayList<Feature>();
//
//		public SearchResultAdapter(Context context)
//		{
//			this.context = context;
//			this.inflater = LayoutInflater.from(this.context);
//		}
//
//		@Override
//		public int getCount()
//		{
//			if (null == featureLst)
//			{
//				return 0;
//			}
//			else
//			{
//				return featureLst.size();
//			}
//		}
//
//		@Override
//		public Object getItem(int position)
//		{
//			if (null == featureLst)
//			{
//				return null;
//			}
//			else
//			{
//				return featureLst.get(position);
//			}
//
//		}
//
//		@Override
//		public long getItemId(int position)
//		{
//			return position;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent)
//		{
//			ViewHolder holder = null;
//
//			if (null == convertView)
//			{
//				holder = new ViewHolder();
//				convertView = inflater.inflate(R.layout.searchresult_list_item, null);
//
//				holder.oidText = (TextView) convertView.findViewById(R.id.oid_tv);
//				holder.nameOneText = (TextView) convertView.findViewById(R.id.fieldname_one_tv);
//				holder.nameTwoText = (TextView) convertView.findViewById(R.id.fieldname_two_tv);
//				holder.nameThreeText = (TextView) convertView.findViewById(R.id.fieldname_three_tv);
//				holder.valueOneText = (TextView) convertView.findViewById(R.id.fieldvalue_one_tv);
//				holder.valueTwoText = (TextView) convertView.findViewById(R.id.fieldvalue_two_tv);
//				holder.valueThreeText = (TextView) convertView.findViewById(R.id.fieldvalue_three_tv);
//				holder.featureDetailImv = (ImageView) convertView.findViewById(R.id.feature_detail_imv);
//
//				convertView.setTag(holder);
//			}
//			else
//			{
//				holder = (ViewHolder) convertView.getTag();
//			}
//
//			final Feature feature = featureLst.get(position);
//			holder.oidText.setText("OID:" + feature.getID());
//			// 获取要素属性
//			Record record = feature.getAtt();
//			// 获取要素字段结构
//			Fields fields = feature.getFields();
//			// 获取字段数目
//			short fieldCount = fields.getFieldCount();
//			//当要素的数目大于3的时候
//			if (fieldCount > 3)
//			{
//				for (short j = 0; j < fieldCount; j++)
//				{
//					Field field = fields.getField(j);
//					if (j == 0)
//					{
//						// 获取字段名称
//						String strFieldName1 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue1 = record.getFldVal(strFieldName1);
//						holder.nameOneText.setText(strFieldName1 + "");
//						holder.valueOneText.setText(fieldValue1 + "");
//					}
//					if (j == 1)
//					{
//						// 获取字段名称
//						String strFieldName2 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue2 = record.getFldVal(strFieldName2);
//						holder.nameTwoText.setText(strFieldName2 + "");
//						holder.valueTwoText.setText(fieldValue2 + "");
//					}
//					if (j == 2)
//					{
//						// 获取字段名称
//						String strFieldName3 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue3 = record.getFldVal(strFieldName3);
//						holder.nameThreeText.setText(strFieldName3 + "");
//						holder.valueThreeText.setText(fieldValue3 + "");
//					}
//
//				}
//
//			}
//			//当要素的数目小于3的时候
//			if(fieldCount <= 3)
//			{
//				holder.featureDetailImv.setVisibility(View.GONE);
//				for(short i = 0;i < fieldCount;i++)
//				{
//					Field field = fields.getField(i);
//					if (i == 0)
//					{
//						// 获取字段名称
//						String strFieldName1 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue1 = record.getFldVal(strFieldName1);
//						holder.nameOneText.setText(strFieldName1 + "");
//						holder.valueOneText.setText(fieldValue1 + "");
//					}
//					if (i == 1)
//					{
//						// 获取字段名称
//						String strFieldName2 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue2 = record.getFldVal(strFieldName2);
//						holder.nameTwoText.setText(strFieldName2 + "");
//						holder.valueTwoText.setText(fieldValue2 + "");
//					}
//					if (i == 2)
//					{
//						// 获取字段名称
//						String strFieldName3 = field.getFieldName();
//						// 获取对应字段的值
//						Object fieldValue3 = record.getFldVal(strFieldName3);
//						holder.nameThreeText.setText(strFieldName3 + "");
//						holder.valueThreeText.setText(fieldValue3 + "");
//					}
//				}
//							
//				
//			}
//			//点击事件
//			holder.featureDetailImv.setOnClickListener(new OnClickListener()
//			{
//				
//				@Override
//				public void onClick(View v)
//				{
//					FeatureDetailFrag featureDetailFrag = new FeatureDetailFrag();
//					Bundle data = new Bundle();
//					data.putSerializable("feature", feature);
//					featureDetailFrag.setArguments(data);
//					mMapActivity.addFragment(EnumViewPos.FULL, featureDetailFrag, featureDetailFrag.getName(), null);
//					mMapActivity.addFragBackListen(featureDetailFrag);
//				}
//			});
//
//			return convertView;
//		}
//
//		public List<Feature> getFeatueLst()
//		{
//			return featureLst;
//		}
//
//		public void setFeatueLst(List<Feature> featueLst)
//		{
//			this.featureLst = featueLst;
//		}
//
//		class ViewHolder
//		{
//			public TextView oidText;
//			public TextView nameOneText;
//			public TextView valueOneText;
//			public TextView nameTwoText;
//			public TextView valueTwoText;
//			public TextView nameThreeText;
//			public TextView valueThreeText;
//			public ImageView featureDetailImv;
//
//		}
//
//	}
//
//	@Override
//	public boolean goback()
//	{
//		return false;
//	}
//
//	@Override
//	public boolean isLocked()
//	{
//		return false;
//	}
//
//	@Override
//	public String getName()
//	{
//		return TAG;
//	}
//
//}
