package com.zondy.mapgis.android.view;

import java.util.List;

import android.content.Context;
import android.location.GpsSatellite;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * 卫星视图
 */
public class SatelliteView extends SurfaceView implements SurfaceHolder.Callback {
	@SuppressWarnings("unused")
	private static final String LOG_TAG = "SatelliteView";
	// 创建DrawSatellitesThread对象
	private DrawSatellitesThread thread;
	private SurfaceHolder surfaceHolder;

	private Context context;

	public SatelliteView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setKeepScreenOn(true);
		this.context = context;
		surfaceHolder = getHolder();
		surfaceHolder.addCallback(this);

	}

	/**
	 * surface大小变化时触发
	 * 
	 * @param holder
	 * @param format
	 * @param width
	 * @param height
	 */
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// 调用DrawSatellitesThread的setSurfaceSize方法
		width = height;
		thread.setSurfaceSize(width, height);
	}

	/**
	 * 获取线程
	 * 
	 * @return thread
	 */
	public DrawSatellitesThread getThread() {
		return thread;
	}

	/**
	 * 创建surface，重建绘图线程。
	 * 
	 * @param holder
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread = new DrawSatellitesThread(surfaceHolder, context);
		thread.setRunning(true);
		thread.start();
	}

	/**
	 * 重画卫星集合
	 * 
	 * @param satellites
	 */
	public void repaintSatellites(List<GpsSatellite> satellites) {
		thread.repaintSatellites(satellites);
	}

	/**
	 * 销毁surface，将当前thread设为不再运行。
	 * 
	 * @param holder
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		thread.setRunning(false);
		thread.interrupt();
		thread = null;
	}
}
