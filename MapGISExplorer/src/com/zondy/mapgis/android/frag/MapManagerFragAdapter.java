package com.zondy.mapgis.android.frag;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class MapManagerFragAdapter extends FragmentPagerAdapter{
	
	
	public final static int TAB_COUNT = 3;
	private List<Fragment> fragments;
	
	public MapManagerFragAdapter(FragmentManager fragmentManager,List<Fragment> fragments) {
		super(fragmentManager);
		this.fragments = fragments;
	}

	@Override
	public Fragment getItem(int id) {
//		switch (id) {
//		case MapManagerModuleFrag.MAPGIS_MAPX:
//			MapxChooseFrag mapxChoose = new MapxChooseFrag();
//			return mapxChoose;
//		case MapManagerModuleFrag.MAPGIS_IGSERVER:
//			IGServerMapFrag igserverFragment = new IGServerMapFrag();
//			return igserverFragment;
//		case MapManagerModuleFrag.IMAGE:
//			MenuRightFragment carFragment = new MenuRightFragment();
//			return carFragment;
//		case MapManagerModuleFrag.ONLINE_MAP:
//			OthersOnlineMapFrag othersMapFragment = new OthersOnlineMapFrag();
//			return othersMapFragment;
//		}
		return fragments.get(id);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}
}
