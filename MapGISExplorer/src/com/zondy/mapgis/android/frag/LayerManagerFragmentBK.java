package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.map.dao.IBaseLayer;
import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.core.map.GroupLayer;
import com.zondy.mapgis.core.map.Map;
import com.zondy.mapgis.core.map.MapLayer;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.Layer;
import com.zondy.mapgis.explorer.R;

/**
 * @content 图层管理Fragment
 * @author fjl 2016-6-12 下午3:46:42
 */
public class LayerManagerFragmentBK extends BaseFragment implements OnClickListener
{

	public String FRAGMENTNAME = this.getClass().getSimpleName();
	private MapActivity mapActivity = null;
	private MapView mMapView = null;
	private Map map = null;

	private TextView tvTransparency;
	private ImageView imgBack;
	private TextView tvTitle;
//	private CheckBox cbSetAllLyrState;
	private CheckBox cbSetSymbolShow;

	private ListView lvLayer;
	private ExpandableListView lvLayerExpand;

	private AdapterLayer adapterLayer;
	private AdapterLayerExpand adapterLayerExpand;

	private List<IBaseLayer> grpLayers;
	private List<ArrayList<IBaseLayer>> childLayers;

	private ArrayList<IBaseLayer> groupLayerList = new ArrayList<IBaseLayer>();
	/**
	 * 第一层是组图层的位置，第二层是该组图层下的子图层
	 */
	private ArrayList<ArrayList<IBaseLayer>> childLayerList = new ArrayList<ArrayList<IBaseLayer>>();
	/**
	 * 为未分组图层预留的自定义分组。
	 */
	private final static String GROUPLAYERFORUNGROUPED_NAME = "分组0";
	private GroupLayer groupLayerForUnGrouped = null;
	private boolean hasUnGrouped = false;
	// 用于记录图层参数是否又更新了,从而决定关闭的时候要不要刷新 (暂未实现)
	private boolean bIsLayerInfoChanged = false;

	@Override
	public int bindLayout()
	{
		return R.layout.maplayers_manager_fragbk;
	}

	@Override
	public void initView(View view)
	{
		mapActivity = MapApplication.getApp().getMapActivity();
		mMapView = mapActivity.mMapView;
		map = mMapView.getMap();

		tvTitle = (TextView) view.findViewById(R.id.tv_subjectmanager_title);
		imgBack = (ImageView) view.findViewById(R.id.img_subjectmanager_back);
		lvLayer = (ListView) view.findViewById(R.id.listview);
		lvLayerExpand = (ExpandableListView) view.findViewById(R.id.listview_expand);
		lvLayerExpand.setGroupIndicator(null);
		lvLayerExpand.setDivider(null);

		imgBack.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.img_subjectmanager_back:
			mapActivity.hideFragment(this.getName(), null);
			break;
		default:
			break;
		}
	}

	@Override
	public void doBusiness(Context mContext)
	{
		int rootLayerCount = map.getLayerCount();
		ArrayList<IBaseLayer> childLayerListForUnGrouped = new ArrayList<IBaseLayer>();

		for (int rootLayerIndex = 0; rootLayerIndex < rootLayerCount; rootLayerIndex++)
		{
			MapLayer rootMapLayer = map.getLayer(rootLayerIndex);
			IBaseLayer rootLayer = new Layer(rootMapLayer);
			if (rootLayer.isGroupLayer())
			{
				this.groupLayerList.add(rootLayer);
				this.childLayerList.add(rootLayer.getChildLayers());
			}
			else
			{
				hasUnGrouped = true;
				childLayerListForUnGrouped.add(rootLayer);
			}
		}
		if (hasUnGrouped)
		{
			this.groupLayerForUnGrouped = new GroupLayer();
			this.groupLayerForUnGrouped.setName(GROUPLAYERFORUNGROUPED_NAME);

			IBaseLayer groupLayer = new Layer(this.groupLayerForUnGrouped);
			this.groupLayerList.add(0, groupLayer);
			this.childLayerList.add(0, childLayerListForUnGrouped);
		}

		 adapterLayer = new AdapterLayer(mapActivity).setListView(lvLayer);
		 adapterLayer.setLayerList(childLayerListForUnGrouped);

		adapterLayerExpand = new AdapterLayerExpand(mapActivity).setListView(lvLayerExpand);
		adapterLayerExpand.setGroupList(groupLayerList);
		adapterLayerExpand.setChildLayerList(childLayerList);

		for (int i = 0; i < childLayerListForUnGrouped.size(); i++)
		{
			Log.d(TAG, "childLayerListForUnGrouped.get(i).getName():" + childLayerListForUnGrouped.get(i).getName());
		}
		for (int i = 0; i < groupLayerList.size(); i++)
		{
			Log.d(TAG, "groupLayerList.get(i).getName():" + groupLayerList.get(i).getName());
		}
	}


	/**
	 * 图层Adapter，分组
	 */
	public class AdapterLayerExpand extends BaseExpandableListAdapter
	{

		private Context context;
		private ExpandableListView listView;

		private List<IBaseLayer> groupLayerList = new ArrayList<IBaseLayer>();
		private List<ArrayList<IBaseLayer>> layerList = new ArrayList<ArrayList<IBaseLayer>>();

		private LayoutInflater inflater = null;

		private Drawable drawableDefault = null;
		private int bgResIdChoose = R.drawable.list_focused_holo;

		/**
		 * 是否处于编辑透明度状态̬
		 */
		private boolean isEditTransparency = false;

		public AdapterLayerExpand(Context context)
		{
			this.context = context;
			this.inflater = LayoutInflater.from(this.context);
		}

		public AdapterLayerExpand setListView(ExpandableListView exLv)
		{
			this.listView = exLv;
			this.listView.setAdapter(this);
			return this;
		}

		public void setGroupList(List<IBaseLayer> grouplayerlist)
		{

			this.groupLayerList = grouplayerlist;
		}

		public void setChildLayerList(List<ArrayList<IBaseLayer>> layerlist)
		{

			this.layerList = layerlist;
		}

		@Override
		public void onGroupCollapsed(int groupPosition)
		{
		}

		@Override
		public void onGroupExpanded(int groupPosition)
		{

		}

		/**
		 * 是否正处于编辑透明度状态̬
		 * 
		 * @return
		 */
		public boolean isEditTransparency()
		{
			return isEditTransparency;
		}
		/**
		 * 设置是否正处于编辑透明度״̬
		 * 
		 * @param isEditTransparency
		 */
		public void setEditTransparency(boolean isEditTransparency)
		{
			this.isEditTransparency = isEditTransparency;
		}
		/**
		 * 选择某一条记录，这个暂时没有正确实现
		 * 
		 * @param index
		 */
		public void select(int indexParent, int indexChild)
		{
			int sizeGrp = getGroupCount();

			int indexGrpLast = -1;
			int indexChildLast = -1;

			int indexGrpNow = indexParent;
			int indexChildNow = indexChild;

			for (int i = 0; i < sizeGrp; i++)
			{
				for (int j = 0; j < getChildrenCount(i); j++)
				{
					if (layerList.get(i).get(j).isDefSelect())
					{
						layerList.get(i).get(j).setDefSelect(false);
						indexGrpLast = i;
						indexChildLast = j;
					}
				}
			}

			layerList.get(indexGrpNow).get(indexChildNow).setDefSelect(true);

			updateSelected(indexGrpLast, indexChildLast, indexGrpNow, indexChildNow);
		}

		private void updateSelected(int indexGrpLast, int indexChildLast, int indexGrpNow, int indexChildNow)
		{
			if (indexGrpLast >= 0 && indexChildLast >= 0)
			{
				getChildView(indexGrpLast, indexChildLast, indexChildLast == getChildrenCount(indexGrpLast) - 1, null, null);
			}
			if (indexGrpNow >= 0 && indexChildNow >= 0)
			{
				getChildView(indexGrpNow, indexChildNow, indexChildNow == getChildrenCount(indexGrpNow) - 1, null, null);
			}
		}

		@Override
		public void notifyDataSetChanged()
		{

			super.notifyDataSetChanged();
		}

		@Override
		public Object getChild(int groupPosition, int childPosition)
		{

			return layerList.get(groupPosition).get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition)
		{

			return childPosition;
		}

		@Override
		public int getChildrenCount(int groupPosition)
		{

			return layerList.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition)
		{

			return null;
		}

		@Override
		public int getGroupCount()
		{

			return groupLayerList.size();
		}

		@Override
		public long getGroupId(int groupPosition)
		{

			return groupPosition;
		}

		@Override
		public View getChildView(final int groupPosition, final int childPosition,
					boolean isLastChild, View convertView, ViewGroup parent) {

			final IBaseLayer layer = layerList.get(groupPosition).get(childPosition);

			LayerViewHolder lvh = new LayerViewHolder();
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_pop_list_item, parent, false);

				lvh.tvName = (TextView) convertView.findViewById(R.id.item_name);
				lvh.cbVisible = (CheckBox) convertView.findViewById(R.id.item_checkbox);
				lvh.sbTransparancy = (SeekBar) convertView.findViewById(R.id.item_seekbar);

				convertView.setTag(lvh);
			} else {
				lvh = (LayerViewHolder) convertView.getTag();
			}

			if (drawableDefault == null) {
				drawableDefault = convertView.getBackground();
			}

			if (layer.isGroupLayer()) {

				convertView.setBackgroundColor(Color.rgb(0xe0, 0xe0, 0xe0));
			}

			lvh.cbVisible.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					CheckBox checkbox = (CheckBox) v;
					boolean visible = checkbox.isChecked();

					layerList.get(groupPosition).get(childPosition).setVisible(visible);

					if (visible) {
						layerList.get(groupPosition).get(childPosition).setVisible(visible);
					}
					AdapterLayerExpand.this.notifyDataSetChanged();
					mMapView.refresh();
				}
			});



			final String strName = layer.getName();

			lvh.tvName.setText(strName);

			lvh.cbVisible.setVisibility(View.VISIBLE);

			if (layer.isDefSelect()) {
				convertView.setBackgroundResource(bgResIdChoose);
			} else {
				convertView.setBackgroundDrawable(drawableDefault);
			}

//			lvh.cbVisible.setChecked(limLst.get(groupPosition).getChildLayerInfo()
//						.get(childPosition).isVisible());

			return convertView;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
		{

			LayerViewHolder lvh = null;
			if (convertView == null)
			{

				convertView = inflater.inflate(R.layout.layout_pop_list_item, parent, false);
				lvh = new LayerViewHolder();
				lvh.tvName = (TextView) convertView.findViewById(R.id.item_name);
				lvh.cbVisible = (CheckBox) convertView.findViewById(R.id.item_checkbox);
				lvh.tvName.setTextColor(context.getResources().getColor(R.color.green));
				lvh.tvName.setTextSize(14f);
				lvh.imgViewIndicator = (ImageView) convertView.findViewById(R.id.indicator_expand);
				if (isExpanded)
				{
					lvh.imgViewIndicator.setImageResource(R.drawable.pic_arrowdown_blue);
				}
				else
				{
					lvh.imgViewIndicator.setImageResource(R.drawable.set_return);
				}
				lvh.imgViewIndicator.setVisibility(View.VISIBLE);
				convertView.setTag(lvh);
			}
			else
			{
				lvh = (LayerViewHolder) convertView.getTag();
			}

			final IBaseLayer layer = groupLayerList.get(groupPosition);

			String strName = layer.getName();
			// TextView editName = (TextView)
			// convertView.findViewById(R.id.item_name);
			lvh.tvName.setText(strName);

			// CheckBox check = (CheckBox)
			// convertView.findViewById(R.id.item_checkbox);
			// RadioButton radio = (RadioButton)
			// convertView.findViewById(R.id.item_radiobutton);

//			lvh.cbVisible.setVisibility(View.VISIBLE);
			lvh.cbVisible.setPadding(5, 0, 0, 0);

			// radio.setId(position);

			lvh.cbVisible.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{

					CheckBox checkbox = (CheckBox) v;
					boolean visible = checkbox.isChecked();
					//
					// limLst.get(groupPosition).setVisible(visible);
					// List<LayerInfoMin> childLims =
					// limLst.get(groupPosition).getChildLayerInfo();
					// for (LayerInfoMin lim : childLims) {
					// lim.setVisible(visible);
					// }
					AdapterLayerExpand.this.notifyDataSetChanged();

					// layer.setVisible(checkbox.isChecked());
					// AdapterLayerExpand.this.notifyDataSetChanged();
				}
			});

			// lvh.cbVisible.setChecked(limLst.get(groupPosition).isVisible());

			return convertView;
		}

		@Override
		public boolean hasStableIds()
		{
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition)
		{
			return true;
		}

	}
	/**
	 * 图层Adapter，未分组
	 */
	public class AdapterLayer extends BaseAdapter
	{

		private Context context;
		private List<IBaseLayer> layerList = new ArrayList<IBaseLayer>();

		private ListView lv;
		private LayoutInflater inflater = null;

		private int bgResIdChoose = R.drawable.list_focused_holo;
		private Drawable drawableDefault = null;

		public AdapterLayer(Context context)
		{
			this.context = context;
			this.inflater = LayoutInflater.from(this.context);
		}

		public AdapterLayer setListView(ListView lv)
		{
			this.lv = lv;
			this.lv.setAdapter(this);
			return this;
		}

		public void setLayerList(List<IBaseLayer> layerList)
		{
			this.layerList = layerList;
		}

		@Override
		public int getCount()
		{
			return layerList.size();
		}

		@Override
		public Object getItem(int position)
		{
			return layerList.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent)
		{

			final IBaseLayer layer = layerList.get(position);

			LayerViewHolder lvh = new LayerViewHolder();
			if (convertView == null)
			{
				convertView = inflater.inflate(R.layout.layout_pop_list_item, parent, false);

				lvh.tvName = (TextView) convertView.findViewById(R.id.item_name);
				lvh.cbVisible = (CheckBox) convertView.findViewById(R.id.item_checkbox);
				lvh.sbTransparancy = (SeekBar) convertView.findViewById(R.id.item_seekbar);

				convertView.setTag(lvh);
			}
			else
			{
				lvh = (LayerViewHolder) convertView.getTag();
			}
			lvh.tvName = (TextView) convertView.findViewById(R.id.item_name);
			lvh.cbVisible = (CheckBox) convertView.findViewById(R.id.item_checkbox);
			lvh.sbTransparancy = (SeekBar) convertView.findViewById(R.id.item_seekbar);

//			lvh.cbVisible.setVisibility(View.VISIBLE);

			if (layer.isGroupLayer())
			{
				convertView.setBackgroundColor(Color.rgb(0xe0, 0xe0, 0xe0));
			}
			if (drawableDefault == null)
			{
				drawableDefault = convertView.getBackground();
			}

			final String strName = layer.getName();

			lvh.tvName.setText(strName);

			lvh.cbVisible.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					CheckBox checkbox = (CheckBox) v;
					boolean visible = checkbox.isChecked();
					layerList.get(position).setVisible(visible);
					AdapterLayer.this.notifyDataSetChanged();
					mMapView.refresh();
				}
			});

			if (layer.isDefSelect())
			{
				convertView.setBackgroundResource(bgResIdChoose);
			}
			else
			{
				convertView.setBackgroundDrawable(drawableDefault);
			}

			// lvh.cbVisible.setChecked(limLst.get(position).isVisible());

			lvh.sbTransparancy.setEnabled(layerList.get(position).isVectorLayer());

			return convertView;
		}

	}
	private static class LayerViewHolder
	{

		public TextView tvName;
		public CheckBox cbVisible;
		public SeekBar sbTransparancy;
		public ImageView imgViewIndicator;
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return FRAGMENTNAME;
	}

}
