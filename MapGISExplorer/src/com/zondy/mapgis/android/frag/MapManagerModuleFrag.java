package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;

/**
 * 地图数据管理Fragment
 * @author fjl
 *
 */
public class MapManagerModuleFrag extends BaseFragment implements OnClickListener
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;

	public static final int MAPGIS_MAPX = 0;
	public static final int MAPGIS_IGSERVER = 1;
	public static final int ONLINE_MAP = 2;

	private ViewPager viewPager;
	private RadioButton mMapxRd, mIGSeverRb, mOnlineMapRb;
	
	private List<Fragment> mFragments = new ArrayList<Fragment>();

	@Override
	public int bindLayout()
	{
		return R.layout.mapdata_manager_frag;
	}

	@Override
	public void initView(View view)
	{
		viewPager = (ViewPager) view.findViewById(R.id.viewpager);
		mMapxRd = (RadioButton)view.findViewById(R.id.rd_mapgis_mapx);
		mIGSeverRb = (RadioButton)view.findViewById(R.id.rd_mapgis_igserver);
		mOnlineMapRb = (RadioButton) view.findViewById(R.id.rd_online_map);
		mMapxRd.setOnClickListener(this);
		mIGSeverRb.setOnClickListener(this);
		mOnlineMapRb.setOnClickListener(this);
		view.findViewById(R.id.close_iv).setOnClickListener(this);

		mFragments.add(new MapxChooseFrag());
		mFragments.add(new IGServerMapFrag());
		mFragments.add(new OthersOnlineMapFrag());
		
		MapManagerFragAdapter adapter = new MapManagerFragAdapter(getChildFragmentManager(),mFragments);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(MAPGIS_MAPX);
		addListener();
		
		mMapActivity = MapApplication.getApp().getMapActivity();
	}

	@Override
	public void doBusiness(Context mContext)
	{

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.rd_mapgis_mapx:
			viewPager.setCurrentItem(MAPGIS_MAPX);
			break;
		case R.id.rd_mapgis_igserver:
			viewPager.setCurrentItem(MAPGIS_IGSERVER);
			break;
		case R.id.rd_online_map:
			viewPager.setCurrentItem(ONLINE_MAP);
			break;
		case R.id.close_iv:
			mMapActivity.removeFragment(this.getName(), null);
			break;
		default:
			break;
		}

	}

	private void addListener()
	{
		viewPager.setOnPageChangeListener(new OnPageChangeListener()
		{
			@Override
			public void onPageSelected(int id)
			{
				switch (id)
				{
				case MAPGIS_MAPX:
					mMapxRd.setChecked(true);
					break;
				case MAPGIS_IGSERVER:
					mIGSeverRb.setChecked(true);
					break;
				case ONLINE_MAP:
					mOnlineMapRb.setChecked(true);
					break;
				default:
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{

			}

			@Override
			public void onPageScrollStateChanged(int arg0)
			{

			}
		});
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
