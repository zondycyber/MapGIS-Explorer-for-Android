//package com.zondy.mapgis.android.frag;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.HashSet;
//
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.transform.TransformerException;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Color;
//import android.location.LocationManager;
//import android.os.CountDownTimer;
//import android.provider.Settings;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Chronometer;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.PopupWindow;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.amap.api.location.AMapLocation;
//import com.amap.api.location.AMapLocationClient;
//import com.amap.api.location.AMapLocationClientOption;
//import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
//import com.amap.api.location.AMapLocationClientOption.AMapLocationProtocol;
//import com.amap.api.location.AMapLocationListener;
//import com.zondy.mapgis.android.activity.MapActivity;
//import com.zondy.mapgis.android.activity.MyTracksActivity;
//import com.zondy.mapgis.android.base.BaseFragment;
//import com.zondy.mapgis.android.gpx.GPXParser;
//import com.zondy.mapgis.android.gpx.bean.GPX;
//import com.zondy.mapgis.android.gpx.bean.Track;
//import com.zondy.mapgis.android.gpx.bean.Waypoint;
//import com.zondy.mapgis.android.graphic.GraphicMultiPoint;
//import com.zondy.mapgis.android.graphic.GraphicPolylin;
//import com.zondy.mapgis.android.mapview.MapView;
//import com.zondy.mapgis.android.utils.DialogUtil;
//import com.zondy.mapgis.android.utils.MapUtil;
//import com.zondy.mapgis.android.utils.PopupWindowUtil;
//import com.zondy.mapgis.android.utils.PopupWindowUtil.PopupWindowConfirmListener;
//import com.zondy.mapgis.android.utils.ToolToast;
//import com.zondy.mapgis.android.utils.ValueUtil;
//import com.zondy.mapgis.core.geometry.Dot;
//import com.zondy.smartmobileapp.MapApplication;
//import com.zondy.mapgis.explorer.R;
//
///**
// * 轨迹记录模块
// * 
// * @author fjl
// * 
// */
//public class RecordTracksFragAMap extends BaseFragment implements OnClickListener
//{
//	private String TAG = this.getClass().getSimpleName();
//	private String TRACKS_PATH = MapApplication.SystemLibraryPath + "Tracks";
//	private MapActivity mMapActivity = null;
//	private MapView mMapView = null;
//	private GraphicMultiPoint mGraphicMultiPoint = null;
//	private GraphicPolylin mGraphicPolylin = null;
//
//	private TextView mTitleTxt = null;
//	private ImageView mTrackSetting = null;
//	private LinearLayout mContent = null;
//	private LinearLayout mRunInfoLayout = null;
//	private ImageView mStartImv = null;
//	private ImageView mStopImv = null;
//	private ImageView mFinishImv = null;
//	private TextView mRouteLength = null;
//	private Chronometer mTimeCm = null;
//	private boolean mIsStart = false;
//	private boolean mIsPause = false;
//	
//
//	private AMapLocationClient locationClient = null;
//	private Track mTrack = new Track();
//	private ArrayList<Waypoint> mWayPoints = null;
//	private boolean IsStartDot = true;
//	private String mStartName = null;
//	private String mTrackDis = null;
//
//	// private AMapLocationClientOption locationOption = new
//	// AMapLocationClientOption();
//
//	@Override
//	public int bindLayout()
//	{
//		return R.layout.record_tracks_frag;
//	}
//
//	@Override
//	public void initView(View view)
//	{
//		mTitleTxt = (TextView) view.findViewById(R.id.title_text);
//		mTrackSetting = (ImageView) view.findViewById(R.id.more_btn);
//		mTitleTxt.setText("记录轨迹");
//		mTrackSetting.setVisibility(View.VISIBLE);
//		mContent = (LinearLayout) view.findViewById(R.id.record_content_layout);
//		mRunInfoLayout = (LinearLayout) view.findViewById(R.id.runinfo_layout);
//		mStartImv = (ImageView) view.findViewById(R.id.start_sport_iv);
//		mStopImv = (ImageView) view.findViewById(R.id.stop_sport_iv);
//		mFinishImv = (ImageView) view.findViewById(R.id.finish_sport_iv);
//		mRouteLength = (TextView) view.findViewById(R.id.route_length_tv);
//		mTimeCm = (Chronometer) view.findViewById(R.id.time_cm);
//
//		view.findViewById(R.id.title_back).setOnClickListener(this);
//		mTrackSetting.setOnClickListener(this);
//		mStartImv.setOnClickListener(this);
//		mStopImv.setOnClickListener(this);
//		mFinishImv.setOnClickListener(this);
//	}
//
//	@Override
//	public void doBusiness(Context mContext)
//	{
//		mMapActivity = MapApplication.getApp().getMapActivity();
//		mMapView = mMapActivity.mMapView;
//		if(mGraphicMultiPoint == null)
//		{
//			mGraphicMultiPoint = new GraphicMultiPoint();
//			mGraphicMultiPoint.setColor(Color.RED);
//			mGraphicMultiPoint.setPointSize(6.0);
//		}
//		if(mGraphicPolylin == null)
//		{
//			mGraphicPolylin = new GraphicPolylin();
//			mGraphicPolylin.setColor(Color.GREEN);
//			mGraphicPolylin.setLineWidth(6.0f);
//		}
//		mMapView.getGraphicLayer().addGraphic(mGraphicMultiPoint);
//		mMapView.getGraphicLayer().addGraphic(mGraphicPolylin);
//		
//		// 初始化定位
//		initLocation();
//		mWayPoints = new ArrayList<Waypoint>();
//	}
//
//	/**
//	 * 初始化定位信息
//	 */
//	public void initLocation()
//	{
//		// 初始化client
//		locationClient = new AMapLocationClient(getContext());
//		// 设置定位参数
//		locationClient.setLocationOption(getDefaultOption());
//		// 设置定位监听
//		locationClient.setLocationListener(locationListener);
//	}
//
//	@Override
//	public void onClick(View v)
//	{
//		switch (v.getId())
//		{
//		case R.id.title_back:
//			mMapActivity.removeFragment(TAG, null);
//			mMapActivity.mMapBottomContent.setVisibility(View.VISIBLE);
//			mMapActivity.mMapRightContent.setVisibility(View.VISIBLE);
//			break;
//		case R.id.start_sport_iv:
//			// 通过系统服务，取得LocationManager对象
//			LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
//			// 判断GPS是否正常启动
//			if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//				ToolToast.showShort("请开启GPS导航...");
//				// 返回开启GPS导航设置界面
//				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//				startActivityForResult(intent, 0);
//				return;
//			}
//			if (mIsStart == false)
//			{
//				mStartImv.setVisibility(View.GONE);
//				mStopImv.setVisibility(View.VISIBLE);
//				mFinishImv.setVisibility(View.VISIBLE);
//				mRunInfoLayout.setVisibility(View.VISIBLE);
//				mContent.setBackgroundColor(Color.WHITE);
//				// 开始导航
//				locationClient.startLocation();
//				mTimeCm.start();
//			}
//			break;
//		case R.id.stop_sport_iv:
//
//			if (mIsPause)
//			{
//				mStopImv.setImageResource(R.drawable.ic_sport_pause);
//				locationClient.startLocation();
//				mIsPause = false;
//			}
//			else
//			{
//				mStopImv.setImageResource(R.drawable.ic_sport_continue);
//				locationClient.stopLocation();
//				mIsPause = true;
//			}
//			break;
//		case R.id.finish_sport_iv:
//			DialogUtil.createTwoButtonDialog(getContext(), "是否保存当前轨迹记录", null, null, null, "确定", "取消", new DialogInterface.OnClickListener()
//			{
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which)
//				{
//					save();
//				}
//			}, null).show();
//			mTimeCm.stop();
//			onDestroyView();
//			mMapActivity.removeFragment(TAG, null);
//			break;
//		case R.id.more_btn:
//			new PopupWindowUtil(new PopupWindowConfirmListener()
//			{
//				@Override
//				public void onPopupWindowConfirm(PopupWindow popupWindow)
//				{
//					mMapActivity.startActivityForResult(new Intent(getContext(), MyTracksActivity.class), mMapActivity.TracksRequestCode);
//					mMapActivity.removeFragment(TAG, null);
//					// startActivity(new Intent(getContext(),
//					// MyTracksActivity.class));
//
//				}
//			}).showTracksSettingPopWindow(mMapActivity, mTrackSetting);
//			break;
//		default:
//			break;
//		}
//
//	}
//
//	/**
//	 * 默认的定位参数
//	 * 
//	 * @since 2.8.0
//	 * @author gaode
//	 * 
//	 */
//	private AMapLocationClientOption getDefaultOption()
//	{
//		AMapLocationClientOption mOption = new AMapLocationClientOption();
////		mOption.setLocationMode(AMapLocationMode.Hight_Accuracy);// 可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
//		mOption.setLocationMode(AMapLocationMode.Device_Sensors);// 可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
//		mOption.setGpsFirst(false);// 可选，设置是否gps优先，只在高精度模式下有效。默认关闭
//		mOption.setHttpTimeOut(30000);// 可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
//		mOption.setInterval(2000);// 可选，设置定位间隔。默认为2秒
//		mOption.setNeedAddress(true);// 可选，设置是否返回逆地理地址信息。默认是ture
//		mOption.setOnceLocation(false);// 可选，设置是否单次定位。默认是false
//		mOption.setOnceLocationLatest(false);// 可选，设置是否等待wifi刷新，默认为false.如果设置为true,会自动变为单次定位，持续定位时不要使用
//		AMapLocationClientOption.setLocationProtocol(AMapLocationProtocol.HTTP);// 可选，
//																				// 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
//		return mOption;
//	}
//
//	/**
//	 * 定位监听
//	 */
//	AMapLocationListener locationListener = new AMapLocationListener()
//	{
//		@Override
//		public void onLocationChanged(AMapLocation location)
//		{
//			if (null != location)
//			{
//				Waypoint waypoint = new Waypoint();
//				Dot startDot = new Dot();
//				Dot endDot = new Dot();
//				if(IsStartDot)
//				{
//					mStartName = location.getPoiName();
//					startDot.x = location.getLongitude();
//					startDot.y = location.getLatitude();
//					IsStartDot = false;
//				}
//				mTrack.setName(mStartName +"-"+location.getPoiName());
//				waypoint.setLatitude(location.getLatitude());
//				waypoint.setLongitude(location.getLongitude());
//				mWayPoints.add(waypoint);
//				MapUtil.drawTracksOnMap(getContext(),mMapView, mGraphicMultiPoint, mGraphicPolylin, mWayPoints);
//				
//				endDot.x = location.getLongitude();
//				endDot.y = location.getLatitude();
//				mTrackDis = getRouteDis(startDot, endDot);
//				mRouteLength.setText(mTrackDis);
//			}
//			else
//			{
//				ToolToast.showShort("定位失败!!");
//			}
//		}
//	};
//	
//	/**
//	 * 保存轨迹文件到本地
//	 */
//	private void save()
//	{
//		try
//		{
//			File file = new File(TRACKS_PATH);
//			if(!file.exists())
//			{
//				//创建文件夹
//				file.mkdirs();
//			}
//			//Track
//			HashSet<Track> trackSet = new HashSet<Track>();
//			mTrack.setTrackPoints(mWayPoints);
//			mTrack.setComment(mTrackDis);
//			trackSet.add(mTrack);
//			GPXParser parser = new GPXParser();
////			ValueUtil.formatUTC(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss")
//			FileOutputStream out = new FileOutputStream(TRACKS_PATH + "/" + mTrack.getName()+"-"+mTrack.getComment()+"-"+mTrack.getDescription()+".gpx");
//			GPX gpx = new GPX();
//			gpx.setVersion("1.0");
//			gpx.setCreator("www.smaryun.com");
//			gpx.setTracks(trackSet);
//			parser.writeGPX(gpx, out);
////			out.flush();
//			out.close();
//		}
//		catch (ParserConfigurationException e)
//		{
//			e.printStackTrace();
//		}
//		catch (TransformerException e)
//		{
//			e.printStackTrace();
//		}
//		catch (FileNotFoundException e)
//		{
//			e.printStackTrace();
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 计算运动过程中的距离
//	 * @param dot1
//	 * @param dot2
//	 * @return
//	 */
//	private String getRouteDis(Dot dot1, Dot dot2)
//	{
//		String strDis = "0.0km";
//		double length = MapUtil.dotDistance(dot1, dot2);
//		DecimalFormat df = new DecimalFormat("###0.0");
//		strDis = df.format(length / 1000) +"km";
//		return strDis;
//		
//	}
//	
//	@Override
//	public void onDestroyView()
//	{
//		if (locationClient != null)
//			{
//				locationClient.onDestroy();
//				locationClient = null;
//			}
//		mMapView.getGraphicLayer().removeAllGraphics();
//		mMapView.refresh();
//		super.onDestroyView();
//	}
//	@Override
//	public boolean goback()
//	{
//		return false;
//	}
//
//	@Override
//	public boolean isLocked()
//	{
//		return false;
//	}
//
//	@Override
//	public String getName()
//	{
//		return TAG;
//	}
//
//}
