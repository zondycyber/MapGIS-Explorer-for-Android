package com.zondy.mapgis.android.frag;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
import com.zondy.mapgis.android.activity.LocationTransActivity;
import com.zondy.mapgis.android.activity.LoginActivity;
import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.activity.MyTracksActivity;
import com.zondy.mapgis.android.activity.RegisterActivity;
import com.zondy.mapgis.android.activity.ThreadFeedbackActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.environment.Environment;
import com.zondy.mapgis.android.environment.Environment.UpdateCallback;
import com.zondy.mapgis.android.internal.utils.ToastUtil;
import com.zondy.mapgis.android.model.EntityUser;
import com.zondy.mapgis.android.utils.SharedUtils;
import com.zondy.mapgis.android.utils.StringUtils;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;
import com.zondy.umeng.share.Defaultcontent;

public class AccountFrag extends BaseFragment implements OnClickListener
{

	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;
	private EntityUser mUser = null;

	private ImageView mTileBack = null;
	private Button mLoginBtn = null;
	private Button mRegisterBtn = null;
	private TextView mAccountDescriptionText = null;
	private TextView mAccountText = null;

	private UMShareListener mShareListener;

	@Override
	public int bindLayout()
	{
		return R.layout.mimepage_layou_frag;
	}

	@Override
	public void initView(View view)
	{
		view.findViewById(R.id.tracks_view_txt).setOnClickListener(this);
		view.findViewById(R.id.my_feedback_txt).setOnClickListener(this);
		view.findViewById(R.id.mine_share_txt).setOnClickListener(this);
		view.findViewById(R.id.location_trans_tv).setOnClickListener(this);
		view.findViewById(R.id.soft_update_txt).setOnClickListener(this);
		mAccountText = (TextView) view.findViewById(R.id.account_txt);
		mLoginBtn = (Button) view.findViewById(R.id.login_btn);
		mRegisterBtn = (Button) view.findViewById(R.id.register_btn);
		mAccountDescriptionText = (TextView) view.findViewById(R.id.user_description_txt);
		mTileBack = (ImageView) view.findViewById(R.id.title_back);

		mTileBack.setOnClickListener(this);
		mLoginBtn.setOnClickListener(this);
		mRegisterBtn.setOnClickListener(this);
		mAccountText.setOnClickListener(this);
	}

	@Override
	public void doBusiness(Context mContext)
	{
		mMapActivity = MapApplication.getApp().getMapActivity();

		initUser();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		initUser();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.my_feedback_txt:
			mUser = SharedUtils.getInstance().getUserInfo(getContext());
			if (StringUtils.isNotEmpty(mUser.getUserName()))
			{
				startActivity(new Intent(getContext(), ThreadFeedbackActivity.class));
			}
			else
			{
				startActivity(new Intent(getContext(), LoginActivity.class));
			}
			break;

		case R.id.mine_share_txt:
			// AndroidShare share = new AndroidShare(getContext(), "heiheihei",
			// "http://ww1.sinaimg.cn/large/bca0b9cfjw1dz9h6ufo8dj.jpg");
			// share.show();
			umengShare();
			break;
		case R.id.tracks_view_txt:
			mMapActivity.startActivityForResult(new Intent(getContext(), MyTracksActivity.class), mMapActivity.TracksRequestCode);
			// mMapActivity.removeFragment(TAG, null);
			// startActivity(new Intent(getApplicationContext(),
			// MyTracksActivity.class));
			break;
		case R.id.location_trans_tv:
			startActivity(new Intent(getActivity(), LocationTransActivity.class));
			break;
		case R.id.title_back:
			mMapActivity.removeFragment(TAG, null);
			break;
		case R.id.login_btn:
			startActivity(new Intent(mMapActivity, LoginActivity.class));
			break;
		case R.id.register_btn:
			startActivity(new Intent(mMapActivity, RegisterActivity.class));
			break;
		case R.id.account_txt:
			SharedUtils.getInstance().clearUserInfo(getContext());
			initUser();
		case R.id.soft_update_txt:
			/**
			 * @content 增加检测版本是否更新，并弹框提醒用户
			 * @author fjl 2017-6-22 下午3:56:58
			 */
			Environment.isVersionUpdate(getActivity(), new UpdateCallback()
			{
				@Override
				public void onComplete(boolean success)
				{
					if (success == false)
					{
						ToastUtil.showToast(getActivity(), "已是最新版本");
					}
				}
			});
			Environment.updateAppVersion(getActivity());
			break;
		default:
			break;
		}
	}

	/**
	 * 初始化用户信息
	 */
	private void initUser()
	{
		mUser = SharedUtils.getInstance().getUserInfo(getContext());
		if (StringUtils.isNotEmpty(mUser.getUserName()))
		{
			mLoginBtn.setVisibility(View.GONE);
			mRegisterBtn.setVisibility(View.GONE);
			mAccountDescriptionText.setVisibility(View.VISIBLE);
			mAccountText.setText("注销");
			mAccountDescriptionText.setText(mUser.getUserName());
		}
		else
		{
			mLoginBtn.setVisibility(View.VISIBLE);
			mRegisterBtn.setVisibility(View.VISIBLE);
			mAccountDescriptionText.setVisibility(View.GONE);
			mAccountText.setText("尚未登陆");
		}
	}

	/**
	 * 友盟分享
	 */
	private void umengShare()
	{
		mShareListener = new CustomShareListener(mMapActivity);

		/* 增加自定义按钮的分享面板 */
		new ShareAction(getContext())
				.setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
				.addButton("复制文本", "复制文本", "umeng_socialize_copy", "umeng_socialize_copy")
				.addButton("复制链接", "复制链接", "umeng_socialize_copyurl", "umeng_socialize_copyurl").setShareboardclickCallback(new ShareBoardlistener()
				{
					@Override
					public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media)
					{
						if (snsPlatform.mShowWord.equals("复制文本"))
						{
							Toast.makeText(getContext(), "复制文本按钮", Toast.LENGTH_LONG).show();
						}
						else if (snsPlatform.mShowWord.equals("复制链接"))
						{
							Toast.makeText(getContext(), "复制链接按钮", Toast.LENGTH_LONG).show();

						}
						else
						{
							UMWeb web = new UMWeb(Defaultcontent.url);
							web.setTitle(Defaultcontent.title);
							web.setDescription(Defaultcontent.text);
							web.setThumb(new UMImage(getContext(),  Defaultcontent.imageurl));
							new ShareAction(getContext()).withMedia(web).setPlatform(share_media).setCallback(mShareListener).share();
						}
					}
				}).open();

//		new ShareAction(getContext()).setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
//				.withTitle(Defaultcontent.title).withText(Defaultcontent.text).withMedia(new UMImage(getContext(), Defaultcontent.imageurl))
//				.withTargetUrl("http://www.smaryun.com/").setCallback(umShareListener).open();
	}

	private static class CustomShareListener implements UMShareListener
	{

		private WeakReference<MapActivity> mActivity;

		private CustomShareListener(MapActivity activity)
		{
			mActivity = new WeakReference(activity);
		}

		@Override
		public void onStart(SHARE_MEDIA platform)
		{

		}

		@Override
		public void onResult(SHARE_MEDIA platform)
		{

			if (platform.name().equals("WEIXIN_FAVORITE"))
			{
				Toast.makeText(mActivity.get(), platform + " 收藏成功啦", Toast.LENGTH_SHORT).show();
			}
			else
			{
				if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS && platform != SHARE_MEDIA.EMAIL && platform != SHARE_MEDIA.FLICKR
						&& platform != SHARE_MEDIA.FOURSQUARE && platform != SHARE_MEDIA.TUMBLR && platform != SHARE_MEDIA.POCKET
						&& platform != SHARE_MEDIA.PINTEREST

						&& platform != SHARE_MEDIA.INSTAGRAM && platform != SHARE_MEDIA.GOOGLEPLUS && platform != SHARE_MEDIA.YNOTE
						&& platform != SHARE_MEDIA.EVERNOTE)
				{
					Toast.makeText(mActivity.get(), platform + " 分享成功啦", Toast.LENGTH_SHORT).show();
				}

			}
		}

		@Override
		public void onError(SHARE_MEDIA platform, Throwable t)
		{
			if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS && platform != SHARE_MEDIA.EMAIL && platform != SHARE_MEDIA.FLICKR
					&& platform != SHARE_MEDIA.FOURSQUARE && platform != SHARE_MEDIA.TUMBLR && platform != SHARE_MEDIA.POCKET
					&& platform != SHARE_MEDIA.PINTEREST

					&& platform != SHARE_MEDIA.INSTAGRAM && platform != SHARE_MEDIA.GOOGLEPLUS && platform != SHARE_MEDIA.YNOTE
					&& platform != SHARE_MEDIA.EVERNOTE)
			{
				Toast.makeText(mActivity.get(), platform + " 分享失败啦", Toast.LENGTH_SHORT).show();

			}

		}

		@Override
		public void onCancel(SHARE_MEDIA platform)
		{

			Toast.makeText(mActivity.get(), platform + " 分享取消了", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
