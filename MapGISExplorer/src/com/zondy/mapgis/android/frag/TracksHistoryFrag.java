package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.activity.MyTracksActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.gpx.bean.Track;
import com.zondy.mapgis.android.gpx.bean.Waypoint;
import com.zondy.mapgis.android.graphic.GraphicImage;
import com.zondy.mapgis.android.graphic.GraphicMultiPoint;
import com.zondy.mapgis.android.graphic.GraphicPolylin;
import com.zondy.mapgis.android.graphic.GraphicsOverlay;
import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.android.utils.MapUtil;
import com.zondy.mapgis.core.geometry.Dot;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;

public class TracksHistoryFrag extends BaseFragment implements OnClickListener
{
	private String TAG = this.getClass().getSimpleName();

	private MapActivity mMapActivity = null;
	private MapView mMapView = null;
	private GraphicsOverlay mGraphicLayer = null;
	private GraphicMultiPoint mGraphicMultiPoint = null;
	private GraphicPolylin mGraphicPolylin = null;
	private GraphicImage mGraphicStartImg = null;
	private GraphicImage mGraphicEndImg = null;
	private Track mTrack = null;
	private ArrayList<Waypoint> mWayPoints = null;
	private Bitmap mStartBmp, mEndBmp;

	private TextView mTitleTxt = null;
	private TextView mStartTxt = null;
	private TextView mEndTxt = null;
	private TextView mRouteLength = null;
	private TextView mTimeTxt = null;

	@Override
	public int bindLayout()
	{
		return R.layout.tracks_histiroy_frag;
	}

	@Override
	public void initView(View view)
	{
		mTitleTxt = (TextView) view.findViewById(R.id.title_text);
		mTitleTxt.setText("历史轨迹");
		mStartTxt = (TextView) view.findViewById(R.id.start_tv);
		mEndTxt = (TextView) view.findViewById(R.id.end_tv);
		mRouteLength = (TextView) view.findViewById(R.id.route_length_tv);
		mTimeTxt = (TextView) view.findViewById(R.id.time_tv);

		view.findViewById(R.id.title_back).setOnClickListener(this);

		Bundle data = getArguments();
		mTrack = (Track) data.get("track");
		// 数据打印测试
		if (mTrack != null)
		{
			if (mTrack.getName() != null)
			{
				String name = mTrack.getName();
				String[] nameTxts = name.split("-");
				if (nameTxts.length > 1)
				{
					mStartTxt.setText(nameTxts[0]);
					mEndTxt.setText(nameTxts[1]);
				}
				else
				{
					mStartTxt.setText(mTrack.getName());
					mEndTxt.setText(mTrack.getName());
				}
			}

			mRouteLength.setText(mTrack.getComment());
			mTimeTxt.setText(mTrack.getDescription());

			Log.d(TAG, "mTrack.getName():" + mTrack.getName());
			Log.d(TAG, "mTrack.getSrc():" + mTrack.getSrc());
			Log.d(TAG, "mTrack.getNumber():" + mTrack.getNumber());
			mWayPoints = mTrack.getTrackPoints();
			for (Waypoint waypoint : mWayPoints)
			{
				Log.d(TAG, "waypoint.getLatitude():" + waypoint.getLatitude());
				Log.d(TAG, "waypoint.getLongitude():" + waypoint.getLongitude());
			}
		}

	}

	@Override
	public void doBusiness(Context mContext)
	{
		mStartBmp = BitmapFactory.decodeResource(getResources(), R.drawable.bubble_start);
		mEndBmp = BitmapFactory.decodeResource(getResources(), R.drawable.bubble_end);
		mMapActivity = MapApplication.getApp().getMapActivity();
		mMapActivity.mMapBottomContent.setVisibility(View.GONE);
		mMapView = mMapActivity.mMapView;
		mGraphicLayer = mMapView.getGraphicsOverlay();

		if (mGraphicMultiPoint == null)
		{
			mGraphicMultiPoint = new GraphicMultiPoint();
			mGraphicMultiPoint.setColor(Color.BLUE);
			mGraphicMultiPoint.setPointSize(12);
		}
		if (mGraphicPolylin == null)
		{
			mGraphicPolylin = new GraphicPolylin();
			mGraphicPolylin.setColor(Color.GREEN);
			mGraphicPolylin.setLineWidth(6.0f);
		}
		if (mGraphicStartImg == null)
		{
			mGraphicStartImg = new GraphicImage();
			mGraphicStartImg.setPoint(new Dot(0.0f, 0.0f));
		}
		if (mGraphicEndImg == null)
		{
			mGraphicEndImg = new GraphicImage();
			mGraphicEndImg.setPoint(new Dot(0.0f, 0.0f));
		}

		mGraphicLayer.addGraphic(mGraphicMultiPoint);
		mGraphicLayer.addGraphic(mGraphicPolylin);
		mGraphicLayer.addGraphic(mGraphicEndImg);
		mGraphicLayer.addGraphic(mGraphicStartImg);
		// 在地图上添加Graphic并放大到当前缩放范围
		List<Dot> mapDotLst = MapUtil.wayPointsToMapDotLst(getContext(), mWayPoints);
		if (null == mapDotLst)
			return;
		MapUtil.drawTracksOnMap(getContext(), mMapView, mGraphicMultiPoint, mGraphicPolylin, mapDotLst);
		MapUtil.zoomToRange(getContext(), mMapView, mapDotLst);
		// addGraphic(mStartBmp, mapDotLst.get(0));
		// addGraphic(mEndBmp, mapDotLst.get(mapDotLst.size() - 1));
		updateGraphicImg(mGraphicStartImg, mStartBmp, mapDotLst.get(0));
		updateGraphicImg(mGraphicEndImg, mEndBmp, mapDotLst.get(mapDotLst.size() - 1));

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.title_back:
			// mMapActivity.startActivityForResult(new Intent(getContext(),
			// MyTracksActivity.class), mMapActivity.TracksRequestCode);
			// mGraphicLayer.removeAllGraphics();
			mMapActivity.startActivityForResult(new Intent(getContext(), MyTracksActivity.class), mMapActivity.TracksRequestCode);
			mMapActivity.removeFragment(TAG, null);
			mGraphicLayer.removeGraphic(mGraphicMultiPoint);
			mGraphicLayer.removeGraphic(mGraphicPolylin);
			mGraphicLayer.removeGraphic(mGraphicStartImg);
			mGraphicLayer.removeGraphic(mGraphicEndImg);
			mMapActivity.mMapBottomContent.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

	/**
	 * 更新地图上添加的GraphicImage
	 * 
	 * @param graphicImage
	 * @param bitmap
	 * @param curPoint
	 */
	public void updateGraphicImg(GraphicImage graphicImage, Bitmap bitmap, Dot curPoint)
	{
		graphicImage.setImage(bitmap);
		graphicImage.setPoint(curPoint);
		graphicImage.setAnchorPoint(new PointF(0.5f, 0.0f));
	}

	/**
	 * 在地图上添加GraphicImage
	 * 
	 * @param bitmap
	 * @param curPoint
	 */
	public void addGraphic(Bitmap bitmap, Dot curPoint)
	{
		GraphicImage graphicImage = new GraphicImage();
		graphicImage.setImage(bitmap);
		graphicImage.setPoint(curPoint);
		graphicImage.setAnchorPoint(new PointF(0.5f, 0.0f));
		mMapView.getGraphicsOverlay().addGraphic(graphicImage);
		mMapView.refresh();
	}

	@Override
	public void onDestroyView()
	{
		// mGraphicLayer.removeAllGraphics();
		mGraphicLayer.removeGraphic(mGraphicMultiPoint);
		mGraphicLayer.removeGraphic(mGraphicPolylin);
		mGraphicLayer.removeGraphic(mGraphicStartImg);
		mGraphicLayer.removeGraphic(mGraphicEndImg);
		mMapView.refresh();
		super.onDestroyView();
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

	@Override
	public void onResume()
	{
		getFocus();
		super.onResume();
	}

	/**
	 * 捕捉返回事件 说明：本方式适用于Fragment页面中没有其他可以获取焦点的View（如EditText）
	 */
	private void getFocus()
	{
		getView().setFocusable(true);
		getView().setFocusableInTouchMode(true);
		getView().requestFocus();
		getView().setOnKeyListener(new View.OnKeyListener()
		{

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK)
				{
					// 监听到返回按钮点击事件
					mMapActivity.startActivityForResult(new Intent(getContext(), MyTracksActivity.class), mMapActivity.TracksRequestCode);
					mGraphicLayer.removeGraphic(mGraphicMultiPoint);
					mGraphicLayer.removeGraphic(mGraphicPolylin);
					mGraphicLayer.removeGraphic(mGraphicStartImg);
					mGraphicLayer.removeGraphic(mGraphicEndImg);
					mMapActivity.removeFragment(TAG, null);
					mMapActivity.mMapBottomContent.setVisibility(View.VISIBLE);
//					return false;// 未处理
					return true;
				}
				return false;
			}
		});
	}

}
