package com.zondy.mapgis.android.frag;

import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.core.attr.Field;
import com.zondy.mapgis.core.attr.Fields;
import com.zondy.mapgis.core.attr.Record;
import com.zondy.mapgis.core.featureservice.Feature;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;

public class FeatureDetailFrag extends BaseFragment
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;
	private Feature mFeature = null;
	private Fields mFields = null;
	private int mId = 0;
	
	private LinearLayout mLinelayout = null; 
	private TextView mTitleTxt = null;
	private TextView mIdTxt = null;
	private TextView mOIDTxt = null;

	@Override
	public int bindLayout()
	{
		return R.layout.feature_detail_frag;
	}

	@Override
	public void initView(View view)
	{
		mLinelayout = (LinearLayout) view.findViewById(R.id.feature_layout);
		mTitleTxt = (TextView) view.findViewById(R.id.title_text);
		mIdTxt = (TextView) view.findViewById(R.id.feature_id_tv);
		mOIDTxt = (TextView) view.findViewById(R.id.feature_oid_tv);
		
		mMapActivity = MapApplication.getApp().getMapActivity();
		
		view.findViewById(R.id.title_back).setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				mMapActivity.removeFragment(TAG, null);
			}
		});
		
		
	}

	@Override
	public void doBusiness(Context mContext)
	{
		Bundle data = getArguments();
		mFeature = (Feature) data.get("feature");
		mFields = (Fields) data.get("fields");
		mId = (Integer) data.get("id");
		mTitleTxt.setText("属性详情");
		mIdTxt.setText(mId+":");
		mOIDTxt.setText("OID = "+mFeature.getID());
		//获取要素的属性
		HashMap<String, String> record = mFeature.getAttributes();
//		Record record = mFeature.getAtt();
		//获取feature字段结构(old)
//		Fields fields = mFeature.getFields();
		//获取feature字段数目
		short featureCount = mFields.getFieldCount();
		for(short i = 0;i < featureCount;i++)
		{
			Field field = mFields.getField(i);
			String fldName = field.getFieldName();
			Object value = record.get(fldName);
			
			//动态添加布局
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);  
			params.setMargins(25, 10, 10, 10); 	
			LinearLayout layout = new LinearLayout(getContext());
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setLayoutParams(params);
			
			TextView tvName = new TextView(getContext());
			tvName.setText(fldName);
			tvName.setTextColor(getResources().getColor(R.color.assist_color_one));
			tvName.setTextSize(15);
			
			LinearLayout.LayoutParams tvparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); 
			tvparams.setMargins(0, 0, 15, 0); 	
			TextView tvValue = new TextView(getContext());
			tvValue.setText(value + "");
			tvValue.setGravity(Gravity.RIGHT);
			tvValue.setLayoutParams(tvparams);
			tvValue.setTextColor(getResources().getColor(R.color.assist_color_one));
			tvValue.setTextSize(15);
			
			layout.addView(tvName);
			layout.addView(tvValue);
			
			ImageView imgeView = new ImageView(getContext());
			imgeView.setBackgroundResource(R.drawable.line);
			
			mLinelayout.addView(layout);
			mLinelayout.addView(imgeView);
		}
		
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
