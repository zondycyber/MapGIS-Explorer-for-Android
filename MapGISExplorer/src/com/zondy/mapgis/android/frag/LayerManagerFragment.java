package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.core.map.GroupLayer;
import com.zondy.mapgis.core.map.Map;
import com.zondy.mapgis.core.map.MapLayer;
import com.zondy.mapgis.core.map.ServerLayer;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;
import com.zondy.treebean.Bean;
import com.zondy.treebean.Node;
import com.zondy.treebean.TreeListViewAdapter;
import com.zondy.treebean.TreeListViewAdapter.OnTreeNodeClickListener;

/**
 * @content 图层管理Fragment
 * @author fjl 2016-6-12 下午3:46:42
 */
public class LayerManagerFragment extends BaseFragment implements OnClickListener
{

	public String FRAGMENTNAME = this.getClass().getSimpleName();
	public MapActivity mapActivity = null;
	public MapView mMapView = null;
	public Map mMap = null;

	public ImageView imgBack;
	public TextView tvTitle;

	private ListView mRootListview = null;
	private List<Bean> mDatas = new ArrayList<Bean>();
	private List<Bean> mDatasTest = new ArrayList<Bean>();
	public TreeListViewAdapter mAdapter = null;

	private List<MapLayer> mLayerLst1 = new ArrayList<MapLayer>();
	private List<MapLayer> mLayerLst2 = new ArrayList<MapLayer>();
	private List<MapLayer> mLayerLst3 = new ArrayList<MapLayer>();
	private List<MapLayer> mLayerLst4 = new ArrayList<MapLayer>();

	@Override
	public int bindLayout()
	{
		return R.layout.maplayers_manager_frag;
	}

	@Override
	public void initView(View view)
	{
		mapActivity = MapApplication.getApp().getMapActivity();
		mMapView = mapActivity.mMapView;
		mMap = mMapView.getMap();

		tvTitle = (TextView) view.findViewById(R.id.tv_subjectmanager_title);
		imgBack = (ImageView) view.findViewById(R.id.img_subjectmanager_back);
		mRootListview = (ListView) view.findViewById(R.id.id_tree);

		imgBack.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.img_subjectmanager_back:
//			mapActivity.hideFragment(this.getName(), null);
			mapActivity.removeFragment(FRAGMENTNAME, null);
			break;
		default:
			break;
		}
	}

	@Override
	public void doBusiness(Context mContext)
	{
		getMapLayerData();
		initDatas();

		try
		{
			mAdapter = new TreeListViewAdapter<Bean>(mRootListview, getContext(), mDatas, 1,mMapView)
			{
				@Override
				public View getConvertView(Node node, int position, View convertView, ViewGroup parent)
				{
					return null;
				}
			};
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		mAdapter.setOnTreeNodeClickListener(new OnTreeNodeClickListener()
		{
			@Override
			public void onClick(Node node, int position)
			{
				if (node.isLeaf())
				{
					// mAdapter.notifyDataSetChanged();
					// Toast.makeText(getContext(), node.getName(),
					// Toast.LENGTH_SHORT).show();
				}
			}

		});
		mRootListview.setAdapter(mAdapter);


	}

	/**
	 * 获取地图图层
	 */
	public void getMapLayerData()
	{
		int rootLayerCount = mMapView.getMap().getLayerCount();
		// 根目录
		for (int rootLayerIndex = 0; rootLayerIndex < rootLayerCount; rootLayerIndex++)
		{
			MapLayer rootMapLayer = mMapView.getMap().getLayer(rootLayerIndex);
			Log.e(TAG, "firstLayer==>:" + rootMapLayer.getName());
			mLayerLst1.add(rootMapLayer);

			if (!(rootMapLayer instanceof ServerLayer) && rootMapLayer instanceof GroupLayer)
			{
				// 二级
				GroupLayer groupLayer2 = (GroupLayer) rootMapLayer;
				for (int b = 0; b < groupLayer2.getCount(); b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					Log.d(TAG, "secondLayer==>:" + secondMapLayer.getName());
					Log.d(TAG, "rootLayerIndex==>:" + rootLayerIndex);
					mLayerLst2.add(secondMapLayer);

					if (!(secondMapLayer instanceof ServerLayer) && secondMapLayer instanceof GroupLayer)
					{
						// 三级
						GroupLayer groupLayer3 = (GroupLayer) secondMapLayer;

						for (int c = 0; c < groupLayer3.getCount(); c++)
						{
							MapLayer threeMapLayer = groupLayer3.item(c);
							Log.e(TAG, "threeMapLayer:" + threeMapLayer.getName());
							Log.e(TAG, "b==>:" + b);
							mLayerLst3.add(threeMapLayer);
							// 四级
							if (!(threeMapLayer instanceof ServerLayer) && threeMapLayer instanceof GroupLayer)
							{
								GroupLayer groupLayer4 = (GroupLayer) threeMapLayer;
								for (int d = 0; d < groupLayer4.getCount(); d++)
								{
									MapLayer fourMaplayer = groupLayer4.item(d);
									Log.d(TAG, "fourMaplayer==>:" + fourMaplayer.getName());
									mLayerLst4.add(fourMaplayer);
								}
							}

						}
					}
				}
			}
			//服务图层（一级）
			if(rootMapLayer instanceof ServerLayer)
			{
				GroupLayer groupLayer2 = (GroupLayer) rootMapLayer;
				Log.e(TAG, "rootMapLayer:"+rootMapLayer);
				for(int b = 0; b < groupLayer2.getCount(); b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					Log.e(TAG, secondMapLayer.getName());
				}
			}
		}
	}

	/**
	 * 初始化地图树数据
	 */
	private void initDatas()
	{
		int layerLst1Size = mLayerLst1.size();
		int layerLst2Size = mLayerLst2.size();
		int layerLst3Size = mLayerLst3.size();

		// 一级
		for (int a = 0; a < mLayerLst1.size(); a++)
		{
			Log.d(TAG, a + ":" + mLayerLst1.get(a).getName());
			mDatas.add(new Bean(a + 1, 0, mLayerLst1.get(a).getName(), mLayerLst1.get(a)));
		}

		// 一级~二级
		for (int a = 0; a < mLayerLst1.size(); a++)
		{
			if (!(mLayerLst1.get(a) instanceof ServerLayer) && mLayerLst1.get(a) instanceof GroupLayer)
			{
				// 二级
				GroupLayer groupLayer2 = (GroupLayer) mLayerLst1.get(a);
				for (int b = 0; b < groupLayer2.getCount(); b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					mDatas.add(new Bean(mDatas.size() + 1, a + 1, secondMapLayer.getName(), secondMapLayer));
				}
			}
			// ServerLayer二级
			if(mLayerLst1.get(a) instanceof ServerLayer)
			{
				GroupLayer groupLayer2 = (GroupLayer) mLayerLst1.get(a);
				for(int b = 0; b < groupLayer2.getCount();b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					mDatas.add(new Bean(mDatas.size() + 1, a + 1, secondMapLayer.getName(), secondMapLayer));
				}
			}

		}

		// 二级~三级
		for (int b = 0; b < mLayerLst2.size(); b++)
		{
			Log.e(TAG, b + "mLayerLst2:" + mLayerLst2.get(b).getName());

			if (!(mLayerLst2.get(b) instanceof ServerLayer) && mLayerLst2.get(b) instanceof GroupLayer)
			{
				GroupLayer groupLayer3 = (GroupLayer) mLayerLst2.get(b);
				for (int c = 0; c < groupLayer3.getCount(); c++)
				{
					//三级
					MapLayer threeMapLayer = groupLayer3.item(c);
					Log.e(TAG, "threeMapLayer:" + threeMapLayer.getName());
					Log.d(TAG, "b==>:" + b);
					mDatas.add(new Bean(mDatas.size() + 1, layerLst1Size + 1 + b, threeMapLayer.getName(), threeMapLayer));
				}

			}
			// ServerLayer三级
			if(mLayerLst2.get(b) instanceof ServerLayer)
			{
				GroupLayer groupLayer3 = (GroupLayer) mLayerLst2.get(b);
				for (int c = 0; c < groupLayer3.getCount(); c++)
				{
					MapLayer threeMapLayer = groupLayer3.item(c);
					Log.e(TAG, "threeMapLayer:" + threeMapLayer.getName());
					Log.d(TAG, "b==>:" + b);
					mDatas.add(new Bean(mDatas.size() + 1, layerLst1Size + 1 + b, threeMapLayer.getName(), threeMapLayer));
				}
			}

		}
		// 三级到四级
		for (int c = 0; c < mLayerLst3.size(); c++)
		{
			Log.e(TAG, c + "mLayerLst3:" + mLayerLst3.get(c).getName());

			if (!(mLayerLst3.get(c) instanceof ServerLayer) && mLayerLst3.get(c) instanceof GroupLayer)
			{
				GroupLayer groupLayer4 = (GroupLayer) mLayerLst3.get(c);
				for (int d = 0; d < groupLayer4.getCount(); d++)
				{
					MapLayer fourMapLayer = groupLayer4.item(d);
					mDatas.add(new Bean(mDatas.size() + 1, layerLst1Size + layerLst2Size + c + 1, fourMapLayer.getName(), fourMapLayer));
				}
			}
		}
		// 四级到五级
		for (int d = 0; d < mLayerLst4.size(); d++)
		{
			Log.e(TAG, d + "mLayerLst4:" + mLayerLst4.get(d).getName());
			if (!(mLayerLst4.get(d) instanceof ServerLayer) && mLayerLst4.get(d) instanceof GroupLayer)
			{
				Log.e(TAG, d + "mLayerLst4 ==> isGrouplayer:" + mLayerLst4.get(d).getName());
				GroupLayer groupLayer5 = (GroupLayer) mLayerLst4.get(d);
				for (int e = 0; e < groupLayer5.getCount(); e++)
				{
					MapLayer mapLayer5 = groupLayer5.item(e);
					mDatas.add(new Bean(mDatas.size() + 1, layerLst1Size + layerLst2Size + layerLst3Size + d + 1, mapLayer5.getName(), mapLayer5));
				}
			}

		}

//		mDatasTest.add(new Bean(1, 0, "根目录1"));
//		mDatasTest.add(new Bean(2, 0, "根目录2"));
//		mDatasTest.add(new Bean(3, 0, "根目录3"));
//		mDatasTest.add(new Bean(4, 0, "根目录4"));
//		mDatasTest.add(new Bean(5, 1, "子目录1-1"));
//		mDatasTest.add(new Bean(6, 1, "子目录1-2"));
//		
//		mDatasTest.add(new Bean(7, 2, "子目录2-1"));
//		mDatasTest.add(new Bean(8, 4, "子目录4-1"));
//		mDatasTest.add(new Bean(9, 4, "子目录4-2"));
//		
//		mDatasTest.add(new Bean(10, 5, "子目录1-1-1"));
//		mDatasTest.add(new Bean(11, 8, "子目录4-1-1"));
//		mDatasTest.add(new Bean(12, 8, "子目录4-1-2"));
//		mDatasTest.add(new Bean(13, 8, "子目录4-1-3"));
//		mDatasTest.add(new Bean(14, 9, "子目录4-2-1"));
//		mDatasTest.add(new Bean(15, 9, "子目录4-2-2"));
//		mDatasTest.add(new Bean(16, 9, "子目录4-2-3"));

	}

	@Override
	public void onDestroyView()
	{
//		mMapView.stopCurRequest(new MapViewStopCurRequestCallback()
//		{
//			@Override
//			public void onDidStopCurRequest()
//			{
//				Document document = (Document) mMapView.getMap().getParent();
//				document.save();
//				mMapView.forceRefresh();
//			}
//		});
		super.onDestroyView();
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return FRAGMENTNAME;
	}

}
