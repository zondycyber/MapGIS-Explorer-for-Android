package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.utils.DialogUtil;
import com.zondy.mapgis.android.utils.SharedUtils;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.MapInfo;
import com.zondy.mapgis.explorer.R;

public class OthersOnlineMapFrag extends BaseFragment
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;
	
	List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
	public static String[] mOlineMapStrs = {"OpenStreetMap标准地图","天地图标准地图","天地图卫星标准地图"}; 
//	public  String[] mMapType = {"OpenStreetStandard","BaiduMap","BaiduSatelliteMap"};
	public  String[] mMapType = {"OpenStreetStandard","Tianditu","TianDiTuSatelliteMap"};
	public  String[] mServerUrl  = {"","http://t0.tianditu.com/vec_c/wmts","http://t0.tianditu.com/img_w/wmts"};
	
	private ListView mListView = null;

	@Override
	public int bindLayout()
	{
		return R.layout.mapx_manager_frag;
	}

	@Override
	public void initView(View view)
	{
		mListView = (ListView) view.findViewById(R.id.files);
	}

	@Override
	public void doBusiness(Context mContext)
	{
		getData();
		mMapActivity = MapApplication.getApp().getMapActivity();
		
		// 定义一个SimpleAdapter
		SimpleAdapter adapter = new SimpleAdapter(getContext(), listItems, R.layout.filelist_item, new String[] { "name", "icon",
				"modify" }, new int[] { R.id.file_name, R.id.icon, R.id.file_modify });
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id)
			{
				DialogUtil.createTwoButtonDialog(getContext(), null, "加载该在线地图文档", null, null, "确定", "取消",
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface arg0, int arg1)
							{
								//更新地图信息
								MapInfo mapInfo = new MapInfo();
								mapInfo.setMapType(MapActivity.ONLINEMAP);
								mapInfo.setMapServerType(mMapType[position]);
								mapInfo.setServerUrl(mServerUrl[position]);
								mapInfo.setServerName(mOlineMapStrs[position]);
								SharedUtils.getInstance().saveMapInfo(getContext(), mapInfo);
								mMapActivity.initMap();
							}
						}, null).show();
			}
		});
	}

	/*
	 * 构造第三方在线地图服务类型
	 */
	public void getData()
	{
		listItems.clear();
		for(int i = 0 ;i < mOlineMapStrs.length; i++)
		{
			 Map<String, Object> listem = new HashMap<String, Object>(); 
			 listem.put("name", mOlineMapStrs[i]);
			 listem.put("modify", mMapType[i]);
			 listItems.add(listem);
		}
		
	}
	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
