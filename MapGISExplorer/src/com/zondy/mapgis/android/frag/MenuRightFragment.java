package com.zondy.mapgis.android.frag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.zondy.mapgis.android.utils.ToolToast;
import com.zondy.mapgis.explorer.R;

public class MenuRightFragment extends Fragment implements OnClickListener {

	private LinearLayout mPathMenu,mRute;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menu_layout_right, null);
		// 初始化界面
		mPathMenu = (LinearLayout) view.findViewById(R.id.pathmenu);
		mRute =  (LinearLayout) view.findViewById(R.id.rute);
		mPathMenu.setOnClickListener(this);
		mRute.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pathmenu:
			ToolToast.showShort("gagaga");
			break;
		case R.id.rute:
			break;
		default:
			break;
		}
	}
}
