package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.map.dao.IListener;
import com.zondy.mapgis.android.utils.DialogUtil;
import com.zondy.mapgis.android.utils.MapLayerUtils;
import com.zondy.mapgis.android.utils.StringUtils;
import com.zondy.mapgis.core.map.MapLayer;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.LandFieldType;
import com.zondy.mapgis.map.model.QueryCondition;
import com.zondy.mapgis.explorer.R;

public class EditAttrConditionsFrag extends BaseFragment implements OnClickListener
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;
	private MapLayer mChooseVtLayer = null;
	private QueryCondition mQueryCondition = null;
	private SearchModuleFrag mSearchModuleFrag = null;
	private String mStrWhere = null;
	private String mStrRelation = null;

	/**
	 * 字段值搜索关系
	 */
	private static final String[] RELATION = { "like", "=", ">", ">=", "<", "<=" };

	private EditText mEtFildName, mEtFildValue, mEtStrWhere;
	private Spinner mSpRelation = null;
	private ArrayAdapter<String> adapterRelation;

	@Override
	public int bindLayout()
	{
		return R.layout.editattr_conditions_frag;
	}

	@Override
	public void initView(View view)
	{
		// 获得从activity中传递过来的值
		Bundle data = getArguments();
		mChooseVtLayer = (MapLayer) data.get("layer");
		mQueryCondition = (QueryCondition) data.get("querycondition");
		mSearchModuleFrag = (SearchModuleFrag) data.get("SearchModuleFrag");

		mEtFildName = (EditText) view.findViewById(R.id.et_field_name);
		mSpRelation = (Spinner) view.findViewById(R.id.sp_field_rela);
		mEtFildValue = (EditText) view.findViewById(R.id.et_filed_value);
		mEtStrWhere = (EditText) view.findViewById(R.id.et_strwhere);

		mEtFildName.setOnClickListener(this);
		view.findViewById(R.id.ok).setOnClickListener(this);
		view.findViewById(R.id.cancle).setOnClickListener(this);
		// view.findViewById(R.id.and_btn).setOnClickListener(this);
		view.findViewById(R.id.and_tv).setOnClickListener(this);
		view.findViewById(R.id.or_tv).setOnClickListener(this);

		adapterRelation = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, RELATION);
		mSpRelation.setAdapter(adapterRelation);
		mSpRelation.setSelection(0);
		mSpRelation.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int posotion, long arg3)
			{
				mStrRelation = (String) mSpRelation.getSelectedItem();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{

			}
		});

	}

	@Override
	public void doBusiness(Context mContext)
	{
		mMapActivity = MapApplication.getApp().getMapActivity();
	}

	@Override
	public void onClick(View v)
	{
		String tempFildName = mEtFildName.getText().toString();
		String tempFildValue = mEtFildValue.getText().toString();
		String tempStr = "";
		if (mStrRelation.equalsIgnoreCase("like"))
		{
			tempFildValue = "'%" + mEtFildValue.getText().toString() + "%'";
		}
		if (StringUtils.isNotEmpty(tempFildName) && StringUtils.isNotEmpty(tempFildValue))
		{
			tempStr = tempFildName + " " + mStrRelation + " " + tempFildValue;
		}
		String strWhere = mEtStrWhere.getText().toString();

		switch (v.getId())
		{
		case R.id.et_field_name:
			onFieldSelectDlg();
			break;

		case R.id.and_tv:
			mEtFildName.setText("");
			mEtFildValue.setText("");
			if (StringUtils.isNullOrZero(strWhere))
			{
				mEtStrWhere.setText(tempStr);
			}
			else
			{
				mEtStrWhere.setText(strWhere + " and " + tempStr);
			}

			break;
		case R.id.or_tv:
			mEtStrWhere.append(" or ");
			break;
		case R.id.ok:
			mMapActivity.removeFragment(TAG, null);
			if (StringUtils.isNullOrZero(strWhere))
			{
				mStrWhere = tempStr;
			}
			else
			{
				mStrWhere = mEtStrWhere.getText().toString();
			}

			mQueryCondition.setStrWhere(mStrWhere);
			mSearchModuleFrag.mStrWhereEt.setText(mStrWhere);
			break;
		case R.id.cancle:
			mMapActivity.removeFragment(TAG, null);
			break;
		default:
			break;
		}
	}

	/**
	 * 字段选择对话框
	 * 
	 * @param indexEdit 选中EditText的位置
	 */
	private void onFieldSelectDlg()
	{
		final List<String> fldNames = new ArrayList<String>();
		final List<LandFieldType> fldTypes = new ArrayList<LandFieldType>();
		
		int ii = MapLayerUtils.getFields(mChooseVtLayer, fldNames, fldTypes);

		DialogUtil.showEditListDlg(getContext(), "选择查询字段", fldNames.toArray(new String[fldNames.size()]), new IListener<Integer>()
		{
			@Override
			public void onCall(Integer index)
			{
				mEtFildName.setText(fldNames.get(index));
				// 如果字段类型是 字符串 类型，则只允许 like 关系
				if (fldTypes.get(index) == LandFieldType.fldString)
				{
					mSpRelation.setSelection(0);
				}
			}
		});

	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
