package com.zondy.mapgis.android.frag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.utils.SharedUtils;
import com.zondy.mapgis.android.utils.StringUtils;
import com.zondy.mapgis.android.view.SpinerAbstractAdapter;
import com.zondy.mapgis.android.view.SpinerPopWindow;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.MapInfo;
import com.zondy.mapgis.explorer.R;

public class IGServerMapFrag extends BaseFragment implements OnClickListener,SpinerAbstractAdapter.IOnItemSelectListener
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;

	private AutoCompleteTextView mAutoIGServerAddress = null;
	private Button mOkBtn = null;
	private ArrayAdapter<String> arr_adapter;

	private List<String> mHistoryLst = new ArrayList<String>();
	private SpinerPopWindow mSpinerPopWindow;
	private EditText mIGServerEt = null;
	private RelativeLayout mRecentLayout = null;

	@Override
	public int bindLayout()
	{
		return R.layout.igserver_manager_frag;
	}

	/**
	 * 初始化窗体
	 */
	@Override
	public void initView(View view)
	{
		mOkBtn = (Button) view.findViewById(R.id.ok_btn);
		mAutoIGServerAddress = (AutoCompleteTextView) view.findViewById(R.id.auto);
		mIGServerEt = (EditText) view.findViewById(R.id.et_igserver_address);
		mRecentLayout = (RelativeLayout) view.findViewById(R.id.recent_layout);
		
		mRecentLayout.setOnClickListener(this);
		mOkBtn.setOnClickListener(this);

	}

	@Override
	public void doBusiness(Context mContext)
	{
		mMapActivity = MapApplication.getApp().getMapActivity();

		// 获取搜索记录文件内容
		SharedPreferences sp = getContext().getSharedPreferences("search_history", Activity.MODE_MULTI_PROCESS);
		String history = sp.getString("history", "暂时没有搜索记录");
		// 用逗号分割内容返回数组
		String[] history_arr = history.split(",");
		// 新建适配器，适配器数据为搜索历史文件内容
		arr_adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, history_arr);

		// 保留前10条数据
		if (history_arr.length > 10)
		{
			String[] newArrays = new String[10];
			// 实现数组之间的复制
			System.arraycopy(history_arr, 0, newArrays, 0, 10);
			arr_adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, history_arr);
		}
		// 设置适配器
		mAutoIGServerAddress.setAdapter(arr_adapter);
		
		
		mHistoryLst = Arrays.asList(history_arr);
		mSpinerPopWindow = new SpinerPopWindow(getContext());
		mSpinerPopWindow.refreshData(mHistoryLst, 0);
		mSpinerPopWindow.setItemListener(this);
	}
	/**
	 * SpinerPopWindow item点击事件
	 */
	@Override
	public void onItemClick(int pos)
	{
		mIGServerEt.setText(mHistoryLst.get(pos));
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.recent_layout:
			mSpinerPopWindow.setWidth(mRecentLayout.getWidth());
			mSpinerPopWindow.showAsDropDown(mRecentLayout);
			break;

		case R.id.ok_btn:
//			mIGServerEt.setText("http://172.28.156.23:6163/igs/rest/mrms/docs/worldMKT");
//			mIGServerEt.setText("http://172.28.156.23:6163/igs/rest/mrms/docs/武汉市区");
//			mIGServerEt.setText("http://192.168.83.222:6163/igs/rest/mrms/tile/武汉市瓦片");
			String igserverStr = mIGServerEt.getText().toString();

			if (StringUtils.isNotEmpty(igserverStr))
			{
				Log.d(TAG, "IGserver:" + igserverStr);
				MapInfo mapInfo = new MapInfo();
				mapInfo.setMapType(MapActivity.IGSERVER);
				mapInfo.setIgserverAddress(igserverStr);
				SharedUtils.getInstance().saveMapInfo(getContext(), mapInfo);
				mMapActivity.initMap();
				saveText(igserverStr);
				mMapActivity.removeAllFragments();
//				 cleanHistory();
			}
			break;

		default:
			break;
		}
	}

	/**
	 * 保存用户输入文字（不重复添加）
	 * 
	 * @param text
	 */
	public void saveText(String text)
	{
		// 获取搜索框信息
		SharedPreferences mysp = getContext().getSharedPreferences("search_history", Activity.MODE_MULTI_PROCESS);
		String old_text = mysp.getString("history", "暂时没有搜索记录,");

		// 利用StringBuilder.append新增内容，逗号便于读取内容时用逗号拆分开
		StringBuilder builder = new StringBuilder(old_text);
		builder.append(text + ",");

		// 判断搜索内容是否已经存在于历史文件，已存在则不重复添加
		if (!old_text.contains(text + ","))
		{
			SharedPreferences.Editor myeditor = mysp.edit();
			myeditor.putString("history", builder.toString());
			myeditor.commit();
			// ToolToast.showShort("添加成功");
		}
		else
		{
//			ToolToast.showShort("已存在");
		}

	}

	/**
	 * 清空用户输入记录
	 * 
	 * @param
	 */
	public void cleanHistory()
	{
		SharedPreferences sp = getContext().getSharedPreferences("search_history", Activity.MODE_MULTI_PROCESS);
		SharedPreferences.Editor editor = sp.edit();
		editor.clear();
		editor.commit();
		// ToolToast.showShort("清除成功");
		super.onDestroy();
	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}


}
