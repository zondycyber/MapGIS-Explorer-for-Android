package com.zondy.dizhi.server.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zondy.mapgis.android.utils.FileOperation;


public class GsonUtil
{
	
	/**
	 * 解析获取周边地质体（String[] json串）
	 * @param jsonData
	 * @return
	 */
	public static  List<String> getLstStr(String jsonData)
	{
		Gson gson = new Gson();
		List<String> result = gson.fromJson(jsonData, new TypeToken<List<String>>()
		{
		}.getType());
		return result;
	}	
	
	/**
	 * 将Json数据解析成相应的映射对象s
	 * @param jsonData
	 * @param type
	 * @return
	 */
	public static <T> T parseJsonWithGson(String jsonData, Class<T> type)
	{
		Gson gson = new Gson();
		T result = gson.fromJson(jsonData, type);
		return result;
	}
	
	/**
	 * 将Json数组解析成相应的映射对象列表(有问题)
	 * @param jsonData
	 * @param type
	 * @return
	 */
	public static <T> List<T> parseJsonArrayWithGson(String jsonData, Class<T> type)
	{
		Gson gson = new Gson();
		List<T> result = gson.fromJson(jsonData, new TypeToken<List<T>>()
		{
		}.getType());
		return result;
	}

	public static List<Map<String, Object>> listKeyMaps(String jsonString)
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try
		{
			Gson gson = new Gson();
			list = gson.fromJson(jsonString, new TypeToken<List<Map<String, Object>>>()
			{
			}.getType());
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}
		return list;
	}
	
	
	/**
	 * 获取URL的二进制
	 * 
	 * @param urlPath
	 * @return byte[]
	 */
	public static byte[] getHtttpUrlBytes(String urlPath)
	{
		HttpURLConnection urlConn = null;
		byte[] data = null;
		try
		{
			URL url = new URL(urlPath);
			// 打开一个HttpURLConnection连接
			 urlConn = (HttpURLConnection) url.openConnection();
			// 设置连接超时时间
			urlConn.setConnectTimeout(6 * 1000);
			// 开始连接
			urlConn.connect();
			int code = urlConn.getResponseCode();
			// 判断请求是否成功
			if (urlConn.getResponseCode() == 200)
			{
				// 获取返回的数据
				data = FileOperation.InputStreamToByte(urlConn.getInputStream());
				// 关闭连接
				urlConn.disconnect();
			}
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			if(urlConn == null)
			{
				urlConn.disconnect();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			if(urlConn == null)
			{
				urlConn.disconnect();
			}
		}

		return data;
	}
	
	/**
	 * 获取二进制转字符
	 * 
	 * @param bytes
	 * @return
	 */
	public static String getBytesToString(byte[] bytes)
	{
		String result = null;

		try
		{
			result = new String(bytes, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return result;
	}
	public static String getBytesToString(byte[] bytes,String charset)
	{
		String result = null;

		try
		{
			result = new String(bytes, charset);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return result;
	}
	
//	JSONObject object = new JSONObject(PaseDataUtil.getBytesToString(t));
//	JSONArray jsonarray = object.getJSONArray("Data");
	
}
