package com.zondy.dizhi.server.util;

import java.io.File;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class InstallApkReceiver extends BroadcastReceiver{
	
	public static final String EXTRA_APKPATH="apkPath";
	public static final String ACTION_INSTALL_APK ="com.zondy.explorer.action.installapk";
	

	@Override
	public void onReceive(Context context, Intent intent) {
		String apkPath = intent.getStringExtra(EXTRA_APKPATH);
		
		installApk(context, apkPath);
	}

	/**
	 * ��װapk
	 * 
	 * @param url
	 */
	public static void installApk(Context context, String apkPath) {
		File apkfile = new File(apkPath);
		if (!apkfile.exists()) {
			return;
		}
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
				"application/vnd.android.package-archive");
		context.startActivity(i);

	}
}
