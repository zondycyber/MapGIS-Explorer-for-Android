package com.zondy.dizhi.server.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.Handler;

public class HttpUtil
{
	// 接口里的方法就是回调方法
	public interface Callback
	{
		void onResponse(byte[] response);
	}
	
	private final static Handler mHandler = new Handler();

	/**
	 * 获取URL的二进制
	 * 
	 * @param urlPath
	 * @return byte[]
	 */
	public static byte[] getHtttpUrlIo(String urlPath)
	{
		HttpURLConnection urlConn = null;
		byte[] data = null;
		try
		{
			URL url = new URL(urlPath);
			// 打开一个HttpURLConnection连接
			urlConn = (HttpURLConnection) url.openConnection();
			// 设置连接超时时间
			urlConn.setConnectTimeout(60 * 1000);
			urlConn.setReadTimeout(60 * 1000);
			urlConn.setInstanceFollowRedirects(true);
			// 开始连接
			urlConn.connect();
			// 判断请求是否成功
			if (urlConn.getResponseCode() == 200)
			{
				// 获取返回的数据
				data = InputStreamToByte(urlConn.getInputStream());
				// 关闭连接
				urlConn.disconnect();
			}
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			if (urlConn == null)
			{
				urlConn.disconnect();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			if (urlConn == null)
			{
				urlConn.disconnect();
			}
		}
		finally
		{
			if (urlConn == null)
			{
				urlConn.disconnect();
			}
		}

		return data;
	}

	/**
	 * 获取URL输入流
	 * 
	 * @param urlPath
	 * @return InputStream (输入流)
	 */
	public static void getHtttpUrlBytes(final String urlPath ,final Callback callback)
	{
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				HttpURLConnection urlConn = null;
				try
				{
					URL url = new URL(urlPath);
					// 打开一个HttpURLConnection连接
					urlConn = (HttpURLConnection) url.openConnection();
					// 设置连接超时时间
					urlConn.setConnectTimeout(60 * 1000);
					urlConn.setReadTimeout(60 * 1000);
					urlConn.setInstanceFollowRedirects(true);
					// 开始连接
					urlConn.connect();
					// 判断请求是否成功
					if (urlConn.getResponseCode() == 200)
					{
						// 获取返回的数据
						InputStream is = urlConn.getInputStream();
						final byte[] bytes = InputStreamToByte(is);
						mHandler.post(new Runnable()
						{
							@Override
							public void run()
							{
								callback.onResponse(bytes);
							}
						});
						// 关闭连接
//						urlConn.disconnect();
					}
				}
				catch (MalformedURLException e)
				{
					e.printStackTrace();
					if (urlConn == null)
					{
						urlConn.disconnect();
					}
				}
				catch (IOException e)
				{
					e.printStackTrace();
					if (urlConn == null)
					{
						urlConn.disconnect();
					}
				}
			}
		}).start();
		
	}

	/**
	 * 输入流转字节流
	 * */
	public static byte[] InputStreamToByte(InputStream is) throws IOException
	{
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int ch;
		while ((ch = is.read(buffer)) != -1)
		{
			bytestream.write(buffer, 0, ch);
		}
		byte data[] = bytestream.toByteArray();
		bytestream.close();
		return data;

	}

	/**
	 * 
	 * @param bytes
	 * @param charset
	 * @return String (返回值说明)
	 */
	public static String getBytesToString(byte[] bytes, String charset)
	{
		String result = null;
		try
		{
			result = new String(bytes, charset);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return result;
	}
}
