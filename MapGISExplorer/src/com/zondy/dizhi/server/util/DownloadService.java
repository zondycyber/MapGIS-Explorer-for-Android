package com.zondy.dizhi.server.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

public class DownloadService extends IntentService {
	public static final String APK_DOWNLOAD_URL = "apkUrl";
	private static final int BUFFER_SIZE = 10 * 1024; // 8k ~ 32K
	private static final String TAG = DownloadService.class.getSimpleName();
	public static final String BROADCAST_TAG = "com.maxtop.downloadbroadcast";
	private NotificationManager mNotifyManager;
	private Builder mBuilder;
	
	public static Intent createIntent(Context context,String apkUrl){
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(APK_DOWNLOAD_URL,apkUrl);
		return intent;
	}

	public DownloadService() {
		super("DownloadService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	

		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder = new NotificationCompat.Builder(this);
		
		String appName=getString(getApplicationInfo().labelRes);
		int iconId=getApplicationInfo().icon;
		
		mBuilder.setContentTitle(appName).setSmallIcon(iconId);
		String urlStr = intent.getStringExtra(APK_DOWNLOAD_URL);
		InputStream in=null;
		FileOutputStream out = null;
		try {
			URL url = new URL(urlStr);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(false);
			urlConnection.setConnectTimeout(600 * 1000);
			urlConnection.setReadTimeout(60 * 1000);
			urlConnection.setRequestProperty("Connection", "Keep-Alive");
			urlConnection.setRequestProperty("Charset", "UTF-8");
			urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");

			urlConnection.connect();
			long bytetotal = urlConnection.getContentLength();
			long bytesum = 0;
			int byteread = 0;
			in = urlConnection.getInputStream();
			File dir = StorageUtils.getCacheDirectory(this);
//			String apkName=urlStr.substring(urlStr.lastIndexOf("/")+1, urlStr.length());
			String apkName="fjl.apk";
			File apkFile = new File(dir, apkName);
			out = new FileOutputStream(apkFile);
			byte[] buffer = new byte[BUFFER_SIZE];

			int oldProgress = 0;

			while ((byteread = in.read(buffer)) != -1) {
				bytesum += byteread;
				out.write(buffer, 0, byteread);

				int progress = (int) (bytesum * 100L / bytetotal);
				// 如果进度与之前进度相等，则更新，如果更新太频则会出现界面卡顿
				if (progress != oldProgress) {
					updateProgress(progress);
//					updateProgressByBroadCast(progress,apkFile.getAbsolutePath());
				}
				oldProgress = progress;
			}
			//下载完成
			mBuilder.setContentText("下载完成").setProgress(0, 0, false);
			
			Intent installAPKIntent = new Intent(Intent.ACTION_VIEW);
			//如果没有设置SDCard写，或者没有sdcard,apk文件存在内存中，授予能安装
			String[] command = {"chmod","777",apkFile.toString()};
			 ProcessBuilder builder = new ProcessBuilder(command);
			 builder.start();

			installAPKIntent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
			//installAPKIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//installAPKIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); 
			//installAPKIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION); 
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, installAPKIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			mBuilder.setContentIntent(pendingIntent);
			Notification noti = mBuilder.build();
			noti.flags = android.app.Notification.FLAG_AUTO_CANCEL;
			mNotifyManager.notify(0, noti);
			
			Intent installBroadCast = new Intent(InstallApkReceiver.ACTION_INSTALL_APK);
			installBroadCast.putExtra(InstallApkReceiver.EXTRA_APKPATH, apkFile.getAbsolutePath());
			sendBroadcast(installBroadCast);

		} catch (Exception e) {
			Log.e(TAG, "download apk file error", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void updateProgress(int progress) {
		//"正在下载:" + progress + "%"
		mBuilder.setContentText("正在下载:" + progress + "%").setProgress(100, progress, false);
		//setContentInent如果设置4.0+上没有问题，4.0以下会报异常
		PendingIntent pendingintent = PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_CANCEL_CURRENT);
		mBuilder.setContentIntent(pendingintent);
		mNotifyManager.notify(0, mBuilder.build());
	}
	
//	private void updateProgressByBroadCast(int progress,String apkFilePath){
//		Intent intent = new Intent(DownloadService.BROADCAST_TAG);
//		intent.putExtra("currentProgress", progress);
//		intent.putExtra("apkFilePath", apkFilePath);
//		this.sendBroadcast(intent);
//	}

}
